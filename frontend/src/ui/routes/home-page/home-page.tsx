import React, {createContext, memo, ReactNode, useContext, useEffect, useState} from "react";
import { useStore} from "react-redux";
import {Redirect, useRouteMatch} from "react-router-dom";
import NavBar from "../../components/bars/nav-bar";
import {UserRole} from "../../utils/constants/permission-constants";
import {RouteType} from "../../utils/types/route-types";
import LeftBar from "../../components/bars/left-bar";
import RouteRenderer from "../../utils/route-renderer";
import Gradebook from "../../components/gradebook/gradebook";

import './home.scss'
import {
    ArchiveIcon,
    BackpackIcon, BellIcon,
    Crosshair2Icon,
    DashboardIcon,  ExitIcon,
    LightningBoltIcon, ListBulletIcon, MixerVerticalIcon, Pencil1Icon, Pencil2Icon, PersonIcon,
    ReaderIcon,TableIcon
} from "@modulz/radix-icons";
import News from "../../components/news/news";
import Profile from "../../components/profile/profile";
import Notifications from "../../components/notifications/notifications";
// import {useMsal} from "@azure/msal-react";
import Logout from "../../components/logout/logout";
import Groups from "../../components/groups/groups";
import Schedule from "../../components/schedule/schedule";
import Users from "../../components/users/users";
import TaskCreator from "../../components/task-creator/task-creator";
import Tasks from "../../components/tasks/tasks";
import Check from "../../components/check/check";
import Deadlines from "../../components/deadlines/deadlines";
import Materials from "../../components/materials/materials";
import Archive from "../../components/materials/archive";
import {useMsal} from "@azure/msal-react";
import {loginRequest} from "../../../../auth/authConfig";
import {callMsGraph} from "../../../../auth/graph";
import {useApiContext} from "../../utils/contexts/api-context";
import {setUserData} from "../../store/actions/user.actions";

const is_teacher = false

const allUsersPermission = [1,2,3,4]
const studentPermission = [1]
// const assistantPermission = [2]
const teacherPermission = [3,4]
const teacherAndAssistantPermission = [2,3,4]

enum BarPosition {
    initial,
    top,
    left
}

export type TabsType = RouteType & {
    permissions: UserRole[],
    position: BarPosition,
    icon?: ReactNode,
    realPath?:string
}

const RedirectToNews : React.FC = () => <Redirect to={'app/news'} />

export const HOME_ROUTES: TabsType[] = [
    {
        path: 'news',
        component: News,
        title: 'Новости',
        permissions: allUsersPermission,
        position:BarPosition.left,
        icon:<ReaderIcon/>
    },
    {
        path: 'gradebook',
        component: Gradebook,
        title: 'Ведомость',
        permissions: allUsersPermission,
        position:BarPosition.left,
        icon:<DashboardIcon/>
    },
    {
        path: 'deadlines',
        component: Deadlines,
        title: 'Активные дедлайны',
        permissions: studentPermission,
        position:BarPosition.left,
        icon:<LightningBoltIcon/>,
    },
    {
        path: 'tasks',
        component: Tasks,
        title: 'Задания',
        permissions: studentPermission,
        position:BarPosition.left,
        icon:<Crosshair2Icon/>,
    },
    {
        path: 'course_material',
        component: Materials,
        title: 'Учебные материалы',
        permissions: allUsersPermission,
        position:BarPosition.left,
        icon:<BackpackIcon/>,
    },
    {
        path: 'schedule',
        component: Schedule,
        title: 'Расписание',
        permissions: allUsersPermission,
        position:BarPosition.left,
        icon:<TableIcon/>,
    },
    {
        path: 'check',
        component: Check,
        title: 'Проверка заданий',
        permissions: teacherAndAssistantPermission,
        position:BarPosition.left,
        icon:<Pencil2Icon/>,
    },
    {
        path: 'groups',
        component: Groups,
        title: 'Группы',
        permissions: allUsersPermission,
        position:BarPosition.left,
        icon:<ListBulletIcon/>,
    },
    {
        path: 'task_creator',
        component: TaskCreator,
        title: 'Редактор заданий',
        permissions: teacherPermission,
        position:BarPosition.left,
        icon:<Pencil1Icon/>,
    },
    {
        path: 'users',
        component: Users,
        title: 'Пользователи',
        permissions: teacherAndAssistantPermission,
        position:BarPosition.left,
        icon:<MixerVerticalIcon/>,
    },
    {
        path: 'archive',
        component: Archive,
        title: 'Архив',
        permissions: teacherPermission,
        position:BarPosition.left,
        icon:<ArchiveIcon/>,
    },
    {
        path: 'news',
        component: Gradebook,
        title: 'Курс: Исследование операций',
        permissions: allUsersPermission,
        position:BarPosition.top,
        icon:<ReaderIcon/>,
    },
    {
        path: 'notifications',
        component: Notifications,
        title: 'Уведомления',
        permissions: allUsersPermission,
        position:BarPosition.top,
        icon:<BellIcon/>,
    },
    {
        path: 'profile/:id',
        realPath:'profile/self',
        component: Profile,
        title: 'Профиль',
        permissions: allUsersPermission,
        position:BarPosition.top,
        icon:<PersonIcon/>
    },
    {
        path: 'logout',
        component: Logout,
        title: 'Выход',
        permissions: allUsersPermission,
        position:BarPosition.top,
        icon:<ExitIcon/>,
    },{
        path: '',
        component: RedirectToNews,
        title: '',
        permissions: allUsersPermission,
        position:BarPosition.initial
    },
]

export const MessageContext = createContext<{
    status:number,
    setError: () => void,
    setSuccess: () => void
} | undefined>(undefined);

export const useMessageContext = (): {
    status:number,
    setError: () => void,
    setSuccess: () => void
} | undefined => useContext(MessageContext);

const homePageFC : React.FC = () => {
    document.title = "Исследование операций"

    // const user: User = useSelector((state: State) => state.user, shallowEqual)
    // const api = useApiContext()
    // const dispatch = useDispatch()
    const store = useStore()
    let { url} = useRouteMatch();

    const api = useApiContext()

    url = url.replace(/\/$/, '');

    const [status, setStatus] = useState<number>(0)

    const setStatusWithTimeout = (status:number) => {
        setStatus(status)

        setTimeout(setStatus.bind(null,0),4000)
    }

    const value = {
        status: status,
        setError: setStatusWithTimeout.bind(null,1),
        setSuccess: setStatusWithTimeout.bind(null,2)
    }


    // const { instance, accounts } = useMsal();

    const [isCollapsed, setIsCollapsed] = useState(false)
    const { instance, accounts } = useMsal();
    const [graphData, setGraphData] = useState<{id?:number, displayName?: string}>({});

    // api?.getStore().dispatch(setUserData({
    //     id: graphData.id || 0,
    //     name: graphData.displayName,
    //     role: 1
    // }))

    const [userRole, setUserRole] = useState(1)

    function RequestProfileData() {
        // Silently acquires an access token which is then attached to a request for MS Graph data
        instance.acquireTokenSilent({
            ...loginRequest,
            account: accounts[0] as any
        }).then((response) => {
            callMsGraph(response.accessToken).then(response => {
                setGraphData(response)
            });
        })
    }

    useEffect(() => {
        RequestProfileData()

        api?.get('users/client').then((data) => {
            const permissionsCompare: {[k in string]: number} = {
                'SUPERVISOR': 4,
                'STUDENT': 1,
                'ASSISTANT': 2,
                'TEACHER': 3
            }

            api?.getStore().dispatch(setUserData({
                id: data.id,
                name: data.name,
                role: permissionsCompare[data.role]
            }))

            setUserRole(store.getState().user.userData.role)
        })
    },[])

    useEffect(() => {
        if (graphData && graphData.id) {
            api?.setCookies('token', is_teacher? '001' : graphData && graphData.id)

            isSettingsCompleted()
        }
    }, [graphData])

    const [isCompleted, setIsCompleted] = useState(false)

    const isSettingsCompleted = () => {
        api?.get(`users`)
            .catch(() => {
                setIsCompleted(true)
            }
        )
    }

    if (isCompleted)
        return <Redirect to={'/first_visit'}/>

    return (
        <div className={'home'}>
            {
                <div className={`status ${status !== 0 && 'active'} ${status == 2 && 'green'} ${status == 1 &&  'red'}`}>
                    {
                        status == 2 && 'Успешно'
                    }
                    {
                        status == 1 && 'Ошибка'
                    }
                </div>
            }
            <NavBar permissions={allUsersPermission}
                    path={url+'/'}
                    setIsCollapsed={setIsCollapsed}
                    isCollapsed={isCollapsed}
                    routes={HOME_ROUTES.filter(item => item.permissions.includes(userRole)).filter((item) => item.position === BarPosition.top)}/>
            <div className={'home__content'}>
                <LeftBar permissions={allUsersPermission}
                         path={url+'/'}
                         setIsCollapsed={setIsCollapsed}
                         isCollapsed={isCollapsed}
                         routes={HOME_ROUTES.filter(item => item.permissions.includes(userRole)).filter((item) => item.position === BarPosition.left)}/>
                <MessageContext.Provider value={value}>
                    <RouteRenderer path={url+'/'}  routes={HOME_ROUTES.filter(item => item.permissions.includes(userRole))} error={true}/>
                </MessageContext.Provider>
            </div>
        </div>
    )
}

const HomePage = memo(homePageFC)

export default HomePage;

