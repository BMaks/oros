import React, {memo, useEffect, useState} from "react"

import './first-visit.scss'
import {callMsGraph} from "../../../../auth/graph";
import {useMsal} from "@azure/msal-react";
import {loginRequest} from "../../../../auth/authConfig";
import {AccountInfo} from "@azure/msal-common";
import {Redirect} from "react-router-dom";
import {useApiContext} from "../../utils/contexts/api-context";

const firstVisitPageFC:React.FC = () => {
    const { instance, accounts } = useMsal();
    const [graphData, setGraphData] = useState<{displayName?:string, mail?:string, id?:string}>({});
    const api = useApiContext()

    function RequestProfileData() {
        // Silently acquires an access token which is then attached to a request for MS Graph data
        instance.acquireTokenSilent({
            ...loginRequest,
            account: accounts[0] as AccountInfo | undefined
        }).then((response) => {
            callMsGraph(response.accessToken).then(response => {
                setGraphData(response)
                api?.setCookies('token', response.id)
            });
        });

        !isCompleted && setTimeout(RequestProfileData, 5000)
    }
    useEffect(
        () => {
            RequestProfileData()
        },[]
    )
    //редирект на аппп если все ок

    useEffect(() => {
        if (graphData && graphData.id) {
            console.log(graphData && graphData.id)

            api?.setCookies('token',graphData && graphData.id) //graphData && graphData.id

            isSettingsCompleted()
        }
    }, [graphData])

    const [isCompleted, setIsCompleted] = useState(false)

    const isSettingsCompleted = () => {
        api?.get(`users`)
            .then(() => {
                    setIsCompleted(true)
                }
            )
    }

    if (isCompleted)
        return <Redirect to={'/'}/>

    const sendReq = () => {
        api?.post('requests/user',{
            type: 'REGISTER_USER',
            source: JSON.stringify({
                id: graphData?.id,
                name: graphData.displayName,
                email: graphData.mail,
                token: graphData?.id,
                role: 'STUDENT'
            })
        })
    }

    return (
        <div className={'first-visit-page'}>
            <div className={'first-visit__info'}>
                <h1>Исследование операций - Вход</h1>
                <h2>
                    Здравствуйте {graphData.displayName} (<span>{graphData.mail}</span>)
                </h2>
                <span>Кажется вас нет в системе!</span>
                <div>
                    <button onClick={sendReq}>
                        Подать заявку на вход
                    </button>
                </div>
            </div>
        </div>
    )
}

const FirstVisitPage = memo(firstVisitPageFC)

export default FirstVisitPage