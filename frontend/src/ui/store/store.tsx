import {createStore, Store} from 'redux';
import {reducer, State} from "./reducers/reducer";
import initialState from "./initialState";

/**
 * Хранилище
 */
const store:Store<State> = createStore(reducer, initialState);

export default store;