import {UserData} from "../../utils/types/user-types";

export const SET_USER_DATA = 'SET_USER_DATA';

export const setUserData = (userData: UserData):{type:string, payload:UserData} => ({
    type: SET_USER_DATA,
    payload: userData
});

