import {User} from "../../utils/types/user-types";
import {Actions} from "../actions/action";
import {Reducer} from "redux";
import {SET_USER_DATA} from "../actions/user.actions";
import initialState from "../initialState";

export type UserState = User

export const user: Reducer<UserState, Actions> = (
    state: UserState = initialState.user,
    action: Actions
) => {
    switch (action.type) {
        case SET_USER_DATA: {
            return {
                ...state,
                isAuthorized: true,
                userData:{...action.payload},
            };
        }
        default: {
            return state;
        }
    }
};