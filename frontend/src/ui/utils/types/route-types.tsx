import React from "react";

export type RouteType = {
    path: string,
    component?: React.FC,
    title?: string,
    isExact?: boolean,
    isStrict?: boolean,
    props?: any,
}