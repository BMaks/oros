import {UserRole} from "../constants/permission-constants";

export type User = {
    isAuthorized: boolean
    userData?:UserData
}

export type UserData = {
    id?:number
    name?:string
    role:UserRole
}