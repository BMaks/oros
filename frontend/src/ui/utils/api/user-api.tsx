import {User} from "../types/user-types";

export interface IUserApi {
    getUserCookies(): { [k in string]: string } | undefined
}

export class UserApi implements IUserApi{
    constructor(user:User) {
        this._user = user
    }

    private _user:User

    getUserCookies():undefined {
        return undefined
    }
}