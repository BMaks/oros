import {createContext, useContext} from "react";

enum Lang {
    RU,
    ENG
}

export const LangContext = createContext<{
    lang: Lang,
    setLang: (lang:Lang) => void
} | undefined>(undefined);

export const useLangContext = (): {
    lang: Lang,
    setLang: (lang:Lang) => void
} | undefined => useContext(LangContext);