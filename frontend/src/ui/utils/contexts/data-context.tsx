import React from "react";
import {ApiContext} from "./api-context";
import {LangContext} from "./lang-context";
import {Api} from "../api/api";
import {Provider} from "react-redux";
import {State} from "../../store/reducers/reducer";
import {Store} from "redux";

enum Lang {
    RU,
    ENG
}

interface Props {
    api: Api,
    lang: Lang,
    setLang: (lang: Lang) => void,
    store:Store<State>,
    children:React.ReactNode,
}

export const DataProvider:React.FC<Props> = (
    {
        api,
        lang,
        setLang,
        store,
        children,
    }:Props) => {
    return (
        <ApiContext.Provider value={api}>
            <LangContext.Provider value={{lang, setLang}}>
                <Provider store={store}>
                    {children}
                </Provider>
            </LangContext.Provider>
        </ApiContext.Provider>
    )
}