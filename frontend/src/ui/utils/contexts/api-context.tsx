import {createContext, useContext} from "react";
import {Api} from "../api/api";

export const ApiContext = createContext<Api | undefined>(undefined);

export const useApiContext = (): Api | undefined => useContext(ApiContext);