import HomePage from "../../routes/home-page/home-page";
import {RouteType} from "../types/route-types";
import FirstVisitPage from "../../routes/home-page/first-visit-page";
import React from "react";
import {Redirect} from "react-router-dom";
import PublicPage from "../../routes/home-page/public-page";
/** @constant
 * Все доступные Роуты в приложении
 */

const RedirectToNews : React.FC = () => <Redirect to={'/app'} />
const RedirectToNews1 : React.FC = () => <Redirect to={'/app_login'} />

export const MAIN_ROUTES: RouteType[] = [{
        path: '',
        component: RedirectToNews,
        title: 'Домашняя страница',
        isExact:true,
    },
    {
        path: 'app',
        component: HomePage,
        title: 'Домашняя страница',
    },
    {
        path: 'first_visit',
        component: FirstVisitPage,
        title: 'Привет!'
    },
]

export const PUBLIC_PAGE_ROUTES: RouteType[] = [{
        path: '',
        component: RedirectToNews1,
        title: 'Домашняя страница',
        isExact:true,
    },
    {
        path: 'app_login',
        component: PublicPage,
        title: 'Домашняя страница',
    },
]
