import {useEffect} from "react";

/** @hook
 * Используй, если на странице есть Якорные ссылки
 * @param history - useHistory()
 */
export const useAnchorScroll = (history:any) => {
    useEffect(() => {
        const hash = history.location.hash
        const element = document.getElementById(hash.substr(1))

        if (hash && element) {
            element.scrollIntoView({behavior: "smooth"})
        }
    }, [history.location.hash])
}