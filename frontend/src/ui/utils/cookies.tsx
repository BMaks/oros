import {EXPIRES, SAME_SITE} from "./constants/cookies";

/**
 * Возвращает куки по имени или undefined
 * @param name - имя куки
 */
export const getCookies = (name:string):string|undefined => {
    const matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([.$?*|{}()[]\\\/+^])/g, '\\$1') + "=([^;]*)"
    ))
    return matches ? decodeURIComponent(matches[1]) : undefined
}

/**
 * Устанавливает куку
 * @param name имя куки
 * @param value значение куки
 * @param options дополнительно
 */
export const setCookies = (name:string, value:any, options:{[k:string]:string|boolean|number} = {}):void => {
    options = {
        path: '/',
        samesite:SAME_SITE,
        ...options
    };

    const exp = new Date(Date.now() + EXPIRES)

    options.expires = exp.toUTCString()

    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

    for (const optionKey in options) {
        updatedCookie += "; " + optionKey;
        const optionValue = options[optionKey];
        if (optionValue !== true) {
            updatedCookie += "=" + optionValue;
        }
    }

    document.cookie = updatedCookie;
}

/**
 * Удаляет куку
 * @param name имя куки
 */
export const deleteCookie = (name:string):void => {
    setCookies(name, "", {
        'max-age': -1
    })
}
