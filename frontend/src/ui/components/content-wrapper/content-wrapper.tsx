import React, { memo} from "react";

type Props = {
    minWidth?: number,
    maxWidth?: number
    maxHeight?: number,
    children: React.ReactNode,
}

const contentWrapperFC:React.FC<Props> = ({minWidth, maxWidth,maxHeight, children}:Props) => {
    const style = {
        minWidth,
        maxWidth,
        maxHeight
    }

    return (
        <div className={`content-wrapper`}>
            <main style={style} className={'content'}>
                {children}
            </main>
        </div>
    )
}

const ContentWrapper = memo(contentWrapperFC)

export default ContentWrapper;