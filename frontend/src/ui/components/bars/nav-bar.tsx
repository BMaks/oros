import React, {memo, useEffect, useState} from "react";
import {UserRole} from "../../utils/constants/permission-constants";
import {Link} from "react-router-dom";
import {useStore} from "react-redux";
import {TabsType} from "../../routes/home-page/home-page";

import './bars.scss'
import {NotificationPost} from "../notifications/notifications";
import {Cross1Icon, HamburgerMenuIcon} from "@modulz/radix-icons";
import {useApiContext} from "../../utils/contexts/api-context";

type Props = {
    permissions: UserRole[],
    routes: TabsType[],
    path:string,
    setIsCollapsed: (_:boolean) => void,
    isCollapsed:boolean
}

const navBarFC:React.FC<Props> = ({permissions, routes, path, setIsCollapsed,isCollapsed}:Props) => {
    const store = useStore()
    const [userRole, setUserRole] = useState(store.getState().user.userData.role)

    store.subscribe(() => {
        setUserRole(store.getState().user.userData.role)
    })

    const api = useApiContext()

    const [notification, setNotification] = useState(0)

    if (!permissions.includes(userRole))
        return null

    const fetchNotification = async () => {
        let test = 0

        await api?.get('notifications/count').then((res) => {
            test = res
        })

        return test
    }

    const updateCount = () => {
        fetchNotification().then((data) => {
            setNotification(data)
        })

        setTimeout(updateCount, 10000)
    }

    useEffect(() => {
        updateCount()
    },[])

    return (
        <nav className={'nav'}>
            <h1 className={'nav__main-link'}><Link to={path + routes[0].path}>{routes[0].title}</Link></h1>
            <ul className={'nav__links'}>
                <div className={'nav__toggle'} onClick={setIsCollapsed.bind(null,!isCollapsed)}><span
                    className={'link-icon'}>
                    {
                        isCollapsed && <HamburgerMenuIcon />
                    }
                    {
                        !isCollapsed && <Cross1Icon />
                    }
                </span></div>
                {
                    routes.filter((_, index) => index > 0).map((item, index) => {
                        if (item.path.includes('notifications') && notification > 0)
                            return <Link key={index} className={'link'} to={item.realPath ? path + item.realPath: path + item.path}><span
                                className={'link-icon'}>{item.icon}</span><span className={'link__text'}>{item.title}</span><span className={'nav__notifications'}>{notification}</span></Link>

                        return <Link key={index} className={'link'} to={item.realPath ? path + item.realPath: path + item.path}><span
                            className={'link-icon'}>{item.icon}</span><span className={'link__text'}>{item.title}</span></Link>
                    })
                }
            </ul>
        </nav>
    )
}

const NavBar = memo(navBarFC)

export default NavBar