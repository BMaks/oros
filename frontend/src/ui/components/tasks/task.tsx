import React, {memo, useEffect, useState} from "react";

import '../task-creator/task.scss'
import {TaskType} from "../task-creator/task-creator";
import {useApiContext} from "../../utils/contexts/api-context";
import {useParams} from "react-router-dom";
import {useMessageContext} from "../../routes/home-page/home-page";
import {useStore} from "react-redux";

type Props = {
    id?:string
}

type StudentTaskType = {
    id:string,
    files?:{
        name:string,
        path:string
    }[],
    description:string,
    deadlineTime:string,
    deadlineDate:string,
    name:string,
    test?:string[],
    type:TaskType,
}

const taskFC:React.FC<Props> = (id) => {
    const store = useStore()
    const [userName, setUserName] = useState(store.getState().user.userData.name)

    store.subscribe(() => {
        setUserName(store.getState().user.userData.name)
    })

    const [task, setTask] = useState<StudentTaskType>()

    const api = useApiContext()
    const [answer, setAnswer] = useState({})

    const fetchData = async () => {
        const data: {
            task: any
        } = {
            task: {}
        }

        await api?.get(`tasks/${id.id}`).then((res) => {
            data.task = res

            return res
        }).then(async (task) => {
            return api?.get(`taskschemas/${task.schema_id}`).then((res) => {
                data.task = {
                    ...data.task,
                    ...res,
                    id:task.id,
                    options: JSON.parse(res.options)
                }

                if (data.task.type === "TEST") {
                    data.task = {
                        ...data.task,
                        test: JSON.parse(res.options).test.map((item: any) => item.question).filter((q: any) => !!q)
                    }
                }

                return task
            })
        }).then(async (task) => {
            await api?.get(`deadlines/${task.deadline_id}`).then((res) => {
                data.task = {
                    ...data.task,
                    deadlineDate: res.time
                }
            })
        })

        return data
    }

    const {setSuccess, setError} = useMessageContext() || {}

    const send = (e:any) => {
        e.preventDefault()

        api?.post('solvedtasks',{
            name: userName,
            taskId: task?.id,
            content: JSON.stringify(answer)
        }).then(() => {
            setSuccess && setSuccess()
        }).catch((e) => {
            setError && setError()
        })
    }

    const set = (qId:any, value:any) => {
        setAnswer({
            ...answer,
            [qId]: value.target.value
        })
    }

    const updateTask = () => {
        fetchData().then((data) => {
            setTask(data.task)
        })
    }

    useEffect(() => {
        updateTask()
    }, [])

    return (
        <div className={'task__content'}>
            <header className={'task__header'}>
                <h3 className={'task__h3'}>
                    Задание: {task?.name}
                </h3>
            </header>
            <form className={'task__data'} onSubmit={send}>
                <div className={'task__item _student'}>
                    <span className={'task-item__name'}>Описание: </span>
                    <span className={'task-item__value'}>{task?.description}</span>
                </div>
                {
                    (task?.type == TaskType.python || task?.type == TaskType.xlsx) &&
                    <>
                        <div className={'task__item  _student'}>
                            <span className={'task-item__name'}>Условия: </span>
                            {
                                task?.files?.map((file, fileIndex) =>
                                    <a key={fileIndex} download={true} href={file.path}
                                       className={'task-item__value'}>{file.name} [Файл]</a>
                                )
                            }
                        </div>
                        <div className={'task__item'}>
                            <span className={'task-item__name'}>Дедлайн: </span>
                            <span className={'task-item__value'}>{task?.deadlineDate} {task?.deadlineTime}</span>
                        </div>
                        <div className={'task__item _option'}>
                            <span className={'task-item__name'}>Загрузите файл решения: </span>
                            <input type={'file'} onChange={set.bind(null,'file')}/>
                        </div>
                    </>
                }
                {
                    task?.type == TaskType.test &&
                    <>
                        {
                            <div>
                                {
                                    task.test?.map((test, testIndex) =>
                                        <div key={testIndex} className={'task__item _test _student'}>
                                            <div className={'task-item__question'}>
                                                <span className={'task-item__name'}>{test}</span>
                                            </div>
                                            <div className={'task-item__answer'}>
                                                <span className={'task-item__name'}>Введите ответ: </span>
                                                <input type={'text'} onChange={set.bind(null, testIndex)} required={true}/>
                                            </div>
                                        </div>
                                    )
                                }
                            </div>
                        }
                    </>
                }
                <div className={'task__item'}>
                    <span className={'task-item__name'}/>
                    <input type={'submit'} className={'button'} value={'Отправить'}/>
                </div>
            </form>
        </div>
    )
}

const Task = memo(taskFC)

export default Task;