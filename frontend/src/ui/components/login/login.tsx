import React, {memo} from "react";

import './login.scss'
import {useMsal} from "@azure/msal-react";
import {loginRequest} from "../../../../auth/authConfig";

const loginFC:React.FC = () => {
    const { instance } = useMsal();
    const req = () => {
        console.log(instance.getAllAccounts())

        instance.loginRedirect(loginRequest).catch(e => {
            console.log(e);
        })
    }

    return (
        <div className={'login'}>
            <div className={'login__content'}>
                <h1 className={'login__h1'}>Исследование операций</h1>
                <div className={'login__years'}>
                    <div className={'login__info'}>
                        <h2 className={'login__h2'}>Год проведения:</h2>
                        <span className={'login__date'}>2021/2022</span>
                    </div>
                </div>
            </div>
            <button className={'login__button'} onClick={req}>Войти</button>
        </div>
    )
}

const Login = memo(loginFC)

export default Login;