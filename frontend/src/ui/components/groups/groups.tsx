import React, {memo, useEffect, useState} from "react";
import {useStore} from "react-redux";

import './groups.scss'
import {Cross2Icon, GearIcon, UpdateIcon} from "@modulz/radix-icons";
import ContentWrapper from "../content-wrapper/content-wrapper";
import {Group, Student} from "../table/table";
import {Link} from "react-router-dom";
import {useApiContext} from "../../utils/contexts/api-context";
import {useMessageContext} from "../../routes/home-page/home-page";
import {string} from "prop-types";

export const DATE = [
    'Понедельник',
    'Вторник',
    'Среда',
    'Четверг',
    'Пятница',
    'Суббота',
]

export const TIME = [
    '1 Пара',
    '2 Пара',
    '3 Пара',
    '4 Пара',
    '5 Пара',
    '6 Пара',
    '7 Пара',
    '8 Пара',
]

const groupsFC:React.FC = () => {
    document.title = "ИО - Группы"
    const api = useApiContext()

    const store = useStore()
    const [userRole, setUserRole] = useState(store.getState().user.userData.role)
    const [userID, setUserID] = useState(store.getState().user.userData.id)
    const [userName, setUserName] = useState(store.getState().user.userData.name)

    store.subscribe(() => {
        setUserRole(store.getState().user.userData.role)
        setUserID(store.getState().user.userData.id)
        setUserName(store.getState().user.userData.name)
    })

    const admin = [3, 4].includes(userRole)
    const assistant = [2, 3, 4].includes(userRole);
    const student = [1].includes(userRole);

    const [viewMode, setViewMode] = useState(false)
    const [adminEditMode, setAdminEditMode] = useState(false)
    const [writeMode, setWriteMode] = useState(false)

    const changeViewMode = () => {
        setViewMode(!viewMode)
        setAdminEditMode(false)
        setWriteMode(false)
    }

    const onAdminEditMode = () => {
        setAdminEditMode(!adminEditMode)
        setViewMode(true)
        setWriteMode(false)
    }

    const toggleWriteMode = () => {
        setWriteMode(!writeMode)
    }

    const [groupName, setGroupName] = useState('Группа N')

    const onChangeGroupName = (val?: { target: { value: string } }) => {
        const value = val?.target.value || ''

        value && setGroupName(value)
    }

    const [groupDate, setGroupDate] = useState(['', ''])

    const onChangeGroupDate = (val?: { target: { value: string, id: string } }) => {
        const value = val?.target.value || ''
        const id = val?.target.id || '0'
        const date = [...groupDate]

        date[parseInt(id)] = value

        setGroupDate(date)
    }

    const [groupTime, setGroupTime] = useState(['', ''])

    const onChangeGroupTime = (val?: { target: { value: string, id: string } }) => {
        const value = val?.target.value || ''
        const id = val?.target.id || '0'
        const time = [...groupTime]

        time[parseInt(id)] = value

        setGroupTime(time)
    }

    const createGroup = () => {
        const data = {
            name: groupName || '',
            events: JSON.stringify(
        {
                lecture: {
                    date: groupDate[0] || DATE[0],
                    time: groupTime[0] || TIME[0]
                },
                seminar: {
                    date: groupDate[1] || DATE[0],
                    time: groupTime[1] || TIME[0]
                }
            })
        }

        api?.post('groups', data).then(() => {
            setSuccess && setSuccess()
        }).catch(() => {
            setError && setError()
        }).then(() => {
            setWriteMode(!writeMode)
            updateAll()
        })


    }

    const [groupType, setGroupType] = useState(false)

    const onChangeGroupType = () => {
        if (groupType) {
            setGroupName('')
            setGroupTime(['', ''])
            setGroupDate(['', ''])
        }

        setGroupType(!groupType)
    }

    const [groups, setGroups] = useState<Group[]>([])
    const [studentsWithoutGroup, setStudentsWithoutGroup] = useState<Student[]>([])
    const [requests, setRequests] = useState<{ to: string, from: string, studentName: string, reqID: string, studentId: string}[]>([])

    const deleteStudent = (studentId: string, groupId: string) => {
        api?.delete(`groups/${groupId}/${studentId}`).then(() => {
            setSuccess && setSuccess()
        }).catch(() => {
            setError && setError()
        }).then(() => {
            updateAll()
        })
    }

    const fetchGroups = async () => {
        const test = {groups:[]}

        await api?.get('groups?showStudents=true&showAll=true').then((res) => {
            test.groups = res
        })

        return test
    }

    const fetchRequests = async () => {
        const test = {requests: []}

        await api?.get('requests').then((res) => {
            test.requests = res.filter((item:any) => item.type === 'ADD_TO_GROUP')
        })

        return test
    }

    const {setSuccess, setError} = useMessageContext() || {}

    const updateAll = () => {
        Promise.all([
            fetchGroups().then((data: { groups: any }) => data.groups.map((item: any) => {
                return {
                    name: item.name,
                    lecture: item.events && JSON.parse(item.events).lecture,
                    seminar: item.events && JSON.parse(item.events).seminar,
                    isMeta: !item.events,
                    students: item.students || [],
                    id: item.id
                }
            })).then((gr) => {
                setGroups(gr)
            }),
            fetchRequests().then((data: { requests: any }) => data.requests.map((item: any) => {
                return {
                    ...JSON.parse(item.source),
                    reqID: item.id
                }
            })).then((rq) => {
                setRequests(rq)
            })
        ]).catch((e) => {
            console.warn(e)
            setError && setError()
        })
    }

    const makeReq = (groupID:string, groupName:string) => {
        api?.post('requests', {
            type: 'ADD_TO_GROUP',
            source: JSON.stringify({
                studentId: userID,
                studentName: userName,
                to: groupName,
                groupId: groupID,
            }),
            userId: userID
        }).then(() => {
            setSuccess && setSuccess()
        }).catch(() => {
            setError && setError()
        })
    }

    const submit = (reqID: string) => {
        api?.post(`requests/submit/${reqID}`).then(() => {
            setSuccess && setSuccess()
        }).catch(() => {
            setError && setError()
        }).then(() => {
            updateAll()
        })
    }

    const cancel = (reqID: string) => {
        api?.delete(`requests/${reqID}`).then(() => {
            setSuccess && setSuccess()
        }).catch(() => {
            setError && setError()
        }).then(() => {
            updateAll()
        })
    }

    const removeGroup = (id: string) => {
        api?.delete(`groups/${id}`).then(() => {
            setSuccess && setSuccess()
        }).catch(() => {
            setError && setError()
        }).then(() => {
            updateAll()
        })
    }

    useEffect(() => {
        updateAll()
    }, [])

    return (
        <ContentWrapper minWidth={900}>
            <article className={'groups'}>
                <header className={'groups-header'}>
                    <div className={'groups-header__info'}>
                        <h2 className={'groups-header__h2'}>Группы</h2>
                    </div>
                    <div className={'groups-header__buttons'}>
                        {
                            admin &&
                            <button className={'button'} onClick={changeViewMode}>
                                {
                                    !viewMode ?
                                        <>Группы</> :
                                        <>Заявки</>
                                }
                            </button>
                        }
                        {
                            adminEditMode &&
                            <button className={'button'} onClick={toggleWriteMode}>
                                Добавить группу
                            </button>
                        }
                        {
                            admin &&
                            <button className={'button'} onClick={onAdminEditMode}>
                                <span className={'icon'}><GearIcon/></span>
                            </button>
                        }
                        <button className={'button'} onClick={updateAll}><span
                            className={'icon'}><UpdateIcon/></span>
                        </button>
                    </div>
                </header>
                {
                    writeMode &&
                    <section className={'groups-writer'}>
                        <div className={'groups-writer__header'}>
                            <h3 className={'groups-writer__h3'}>Создание группы:</h3>
                            <button className={'button'} onClick={createGroup}>
                                Создать
                            </button>
                        </div>
                        <div className={'groups-writer__inputs'}>
                            <div className={'input-data'}>
                                <span>Введите название новой группы: </span>
                                <input type={'text'} onChange={onChangeGroupName} defaultValue={'Группа N'}/>
                            </div>
                            <div className={'input-data'}>
                                <span> Отображать в расписании: </span>
                                <input className={'checkbox'} type={'checkbox'} onChange={onChangeGroupType}/>
                            </div>
                            {
                                groupType &&
                                ['Лекции', 'Семинара'].map((item, index) =>
                                    <div key={index} className={'input-data'}>
                                        <span>Выберите день проведения <b>{item}</b>: </span>
                                        <select className={'select'} id={index.toString()} onChange={onChangeGroupDate}>
                                            {
                                                DATE.map((item, index) =>
                                                    <option key={item + index} value={item}>{item}</option>
                                                )
                                            }
                                        </select>
                                        <span>и время: </span>
                                        <select className={'select'} id={index.toString()} onChange={onChangeGroupTime}>
                                            {
                                                TIME.map((item, index) =>
                                                    <option key={item + index} value={item}>{item}</option>
                                                )
                                            }
                                        </select>
                                    </div>
                                )
                            }
                        </div>
                    </section>
                }
                {
                    viewMode &&
                    <section className={'group-list'}>
                        <div className={'group-list__header'}>
                            <h3 className={'group-list__h3'}>Список групп:</h3>
                        </div>
                        {
                            groups.map((group, index) =>
                                <div key={index} className={'group'}>
                                    <div className={'group__name'}>
                                        <h4 className={'group-list__h4'}>{group.name}:</h4>
                                        {
                                            adminEditMode && <button className={'button'} onClick={removeGroup.bind(null, group.id)}>Удалить</button>
                                        }
                                    </div>
                                    <div className={'group__data'}>
                                        <div className={'group__info'}>
                                                <span className={'group__span'}> Тип группы:
                                                    {
                                                        group.isMeta ? ' Без расписания' : ' Обычная'
                                                    }.
                                                </span>
                                            {
                                                !group.isMeta &&
                                                <>
                                                        <span className={'group__span'}> Лекция в
                                                            {
                                                                ' ' + group.lecture?.date
                                                            },
                                                            {
                                                                ' ' + group.lecture?.time
                                                            }.
                                                        </span>
                                                    <span className={'group__span'}> Семинар в
                                                        {
                                                            ' ' + group.seminar?.date
                                                        },
                                                        {
                                                            ' ' + group.seminar?.time
                                                        }.
                                                        </span>
                                                </>
                                            }
                                        </div>
                                        <div className={'group__students'}>
                                            {
                                                group.students.map((student, studentIndex) =>
                                                    <div key={studentIndex} className={'student'}>
                                                        <Link to={'/app/profile/' + student.id}>{student.name}</Link>
                                                        {
                                                            adminEditMode &&
                                                            <div
                                                                onClick={deleteStudent.bind(null,student.id, group.id)}
                                                                className={'button'}><span
                                                                className={'icon'}><Cross2Icon/></span>
                                                            </div>}
                                                    </div>
                                                )
                                            }
                                        </div>
                                    </div>
                                </div>
                            )
                        }

                    </section>
                }
                {
                    !viewMode && assistant &&
                    <section className={'requests-list'}>
                        <div className={'requests__header'}>
                            <h3 className={'requests__h3'}>Активные заявки:</h3>
                        </div>
                        {
                            requests.map((request, requestId) =>
                                <div className={'request'} key={requestId}>
                                    <div className={'request__content'}>
                                        <Link className={'request__name'}
                                              to={'/app/profile/' + request.studentId}>{request.studentName}</Link>
                                        <span className={'request__data'}> в <span
                                            className={'span'}>{request.to}</span></span>
                                    </div>
                                    <div className={'request__buttons'}>
                                        <button className={'button'} onClick={submit.bind(null,request.reqID)}>
                                            Принять
                                        </button>
                                        <button className={'button'} onClick={cancel.bind(null,request.reqID)}>
                                            Отклонить
                                        </button>
                                    </div>
                                </div>
                            )
                        }
                        {
                            false && <>
                                <div className={'requests__header'}>
                                    <h3 className={'requests__h3'}>Студенты без группы:</h3>
                                </div>
                                {
                                    studentsWithoutGroup.map((student, studentId) =>
                                        <div className={'request'} key={studentId}>
                                            <div className={'request__content'}>
                                                <Link className={'request__name'}
                                                      to={student.id}>{student.name}</Link>
                                            </div>
                                            <div className={'request__buttons'}>
                                                Отправить в
                                                {
                                                    groups.map((group, groupId) =>
                                                        <button key={groupId} className={'button'}
                                                                onClick={createGroup}>
                                                            {group.name}
                                                        </button>
                                                    )
                                                }
                                            </div>
                                        </div>
                                    )
                                }
                            </>
                        }
                    </section>
                }
                {
                    student &&
                    <section className={'group-list'}>
                        <div className={'group-list__header'}>
                            <h3 className={'group-list__h3'}>Список групп:</h3>
                        </div>
                        {
                            groups.filter(group => !group.isMeta).map((group, index) =>
                                <div key={index} className={'group'}>
                                    <div className={'group__name'}>
                                        <h3 className={'group-list__h4'}>{group.name}:</h3>
                                        <button className={'button'} onClick={makeReq.bind(null, group.id, group.name)}>
                                            Подать заявку
                                        </button>
                                    </div>
                                    <div className={'group__data'}>
                                        <div className={'group__info'}>
                                            {
                                                !group.isMeta &&
                                                <>
                                                    <span className={'group__span'}> Лекция в
                                                        {
                                                            ' ' + group.lecture?.date
                                                        },
                                                        {
                                                            ' ' + group.lecture?.time
                                                        }.
                                                    </span>
                                                    <span className={'group__span'}> Семинар в
                                                        {
                                                            ' ' + group.seminar?.date
                                                        },
                                                        {
                                                            ' ' + group.seminar?.time
                                                        }.
                                                    </span>
                                                </>
                                            }
                                        </div>
                                        <div className={'group__students'}>
                                            {
                                                group.students.map((student, studentIndex) =>
                                                    <div key={studentIndex} className={'student'}>
                                                        <Link to={student.id}>{student.name}</Link>
                                                    </div>
                                                )
                                            }
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                    </section>
                }
            </article>
        </ContentWrapper>
    )
}

const Groups = memo(groupsFC)

export default Groups;














