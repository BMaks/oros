import React, {memo, useEffect, useState} from "react";

import './materials.scss'
import ContentWrapper from "../content-wrapper/content-wrapper";
import {Cross2Icon, UpdateIcon} from "@modulz/radix-icons";
import {useStore} from "react-redux";
import Question from "../question/question";
import {useApiContext} from "../../utils/contexts/api-context";
import {useMessageContext} from "../../routes/home-page/home-page";
import {useUpload} from "react-use-upload";
import {BACKEND_URL} from "../../utils/constants/constants";
import {file} from "@babel/types";

type Material = {
    id:string,
    name: string,
    description: string,
    option: any[],
    active: boolean
}

const materialsFC : React.FC = () => {
    document.title = "ИО - Материалы курса"

    const store = useStore()
    const [userRole, setUserRole] = useState(store.getState().user.userData.role)

    store.subscribe(() => {
        setUserRole(store.getState().user.userData.role)
    })

    const admin = [3, 4].includes(userRole)

    const [addMode, setAddMode] = useState(false)
    const [materials, setMaterials] = useState<Material[]>()

    const changeAddMode = () => {
        setAddMode(!addMode)
    }

    const [materialForDelete, setMaterialForDelete] = useState<Material | undefined>(undefined)

    const confirmDeleteMaterial = (material: Material) => {
        setMaterialForDelete(material)
    }

    const deleteMaterial = () => {
        api?.delete(`materials/${materialForDelete?.id}`).then(() => {
            setSuccess && setSuccess()
        }).catch(() => {
            setError && setError()
        }).then(() => {
            updateMaterials()
        })
    }

    const cancelDeleteMaterial = () => {
        setMaterialForDelete(undefined)
    }

    const addToArchive = (material: Material) => {
        api?.put(`materials/${material.id}`, {
            ...material,
            option: undefined,
            active: false
        }).then(() => {
            setSuccess && setSuccess()
        }).catch(() => {
            setError && setError()
        }).then(() => {
            updateMaterials()
        })
    }

    const {setSuccess, setError} = useMessageContext() || {}

    const [upload,] = useUpload(({files, xhr}) => {
            xhr.withCredentials = true
            const formData = new FormData();

            Array.from(files).forEach((file) => formData.append('file', file));

            xhr.onload = () => updateMaterials()

            return {
                method: "POST",
                url: `${BACKEND_URL}api/v1/materials/${isFilesFlag.id}/files/upload`,
                body: formData,
            }
        }
    );

    const [isFilesFlag, setIsFilesFlag] = useState<{ files?: any, id?: string }>({})

    const addMaterial = (event: any) => {
        event.preventDefault()

        const files = event.target[2].files

        const data: any = {
            name: event.target[0].value,
            description: event.target[1].value,
            active: true,
        }

        api?.post('materials', data).then(async (res) => {
            setIsFilesFlag({id: res.id, files: files})

        }).then(() => {
            setSuccess && setSuccess()
        }).catch(() => {
            setError && setError()
        }).then(() => {
            const newMaterials: Material[] | undefined = materials?.slice().reverse()
            data.option = data?.option?.map((_: any, index: number) => index.toString()) || []
            newMaterials?.push(data)
            newMaterials?.reverse()

            setMaterials(newMaterials)
            setAddMode(false)
        }).then(() => {
            updateMaterials()
        })
    }

    useEffect(() => {
        upload({files: isFilesFlag.files})
    }, [isFilesFlag, setIsFilesFlag])

    const api = useApiContext()

    const fetchData = async () => {
        const data: {
            materials: Material[]
        } = {materials: []}

        await api?.get('materials').then((res) => {
            data.materials = res.filter((item: any) => item.active).reverse()

            return data.materials.map(item => item.id)
        }).then((res) => {
            return Promise.all(res.map((item) => api?.get(`materials/${item}/files`))).then((files) => {
                return files.map((item, index) => {
                    return {item, id: res[index]}
                })
            })
        }).then((res) => {
            data.materials = data.materials.map((item, index) => {
                return {
                    ...item,
                    option: res[index].item.map((file:any) => {

                        return {
                            name: file.name,
                            link: `${BACKEND_URL}api/v1/files/${file.id}/download`
                        }
                    })
                }
            })
        })

        return data
    }

    const updateMaterials = () => {
        fetchData().then((data) => {
            setMaterials(data.materials)
        })
    }

    useEffect(() => {
        updateMaterials()
    }, [])

    return (
        <ContentWrapper minWidth={600} maxWidth={900}>
            <article className={'materials'}>
                <header className={'materials__header'}>
                    <div className={'materials__info'}>
                        <h2 className={'materials__h2'}>
                            Материалы курса
                        </h2>
                    </div>
                    <div className={'materials__buttons'}>
                        {
                            admin &&
                            <button className={'button'} onClick={changeAddMode}>
                                Добавить
                            </button>
                        }
                        <button className={'button'} onClick={updateMaterials}>
                            <span className={'icon'}>
                                <UpdateIcon/>
                            </span>
                        </button>
                    </div>
                </header>
                <section className={'materials__data'}>
                    {
                        addMode &&
                        <form className={'materials__add'} onSubmit={addMaterial}>
                            <input placeholder={'Введите название материала'} type={'text'} required={true}/>
                            <textarea placeholder={'Введите описание материала'}/>
                            <div className={'materials__buttons'}>
                                <input type={'file'} multiple={true}/>
                                <input type={'submit'}/>
                            </div>
                        </form>
                    }
                    {
                        materials &&
                        materials.map((material, materialIndex) =>
                            <div className={'material'} key={materialIndex}>
                                <div className={'material__header'}>
                                    <span className={'material__title'}>{material.name}</span>
                                    {
                                        admin &&
                                        <div className={'material__buttons'}>
                                            <button className={'button'} onClick={addToArchive.bind(null, material)}>В
                                                архив
                                            </button>
                                            <div className={'button'}
                                                 onClick={confirmDeleteMaterial.bind(null, material)}><span
                                                className={'icon'}><Cross2Icon/></span>
                                            </div>
                                        </div>
                                    }
                                </div>
                                {
                                    material.description &&
                                    <pre className={'material__description'}>{material.description}</pre>
                                }
                                {
                                    material?.option?.length > 0 &&
                                    <div className={'material__files'}>
                                        {
                                            material?.option?.map((file, fileIndex) =>
                                                <a key={fileIndex} href={file.link} download={true}>{file.name}</a>)
                                        }
                                    </div>
                                }
                            </div>
                        )
                    }
                    {
                        materials?.length == 0 &&
                        <div className={'materials__empty'}>
                            Нет доступных материалов.
                        </div>
                    }
                </section>
            </article>
            <Question trigger={materialForDelete} message={'Уверены, что хотите удалить материал'}
                      buttonMessage={'Продолжить'} action1={cancelDeleteMaterial} action2={deleteMaterial}/>
        </ContentWrapper>
    )
}

const Materials = memo(materialsFC)

export default Materials;