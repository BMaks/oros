import React, {memo, useEffect, useState} from "react";

import './materials.scss'
import ContentWrapper from "../content-wrapper/content-wrapper";
import {Cross2Icon, UpdateIcon} from "@modulz/radix-icons";
import {useStore} from "react-redux";
import Question from "../question/question";
import {useApiContext} from "../../utils/contexts/api-context";
import {useMessageContext} from "../../routes/home-page/home-page";
import {BACKEND_URL} from "../../utils/constants/constants";

type Material = {
    id:string,
    name: string,
    description: string,
    option: any[],
    active: boolean
}

const archiveFC : React.FC = () => {
    document.title = "ИО - Архив"

    const store = useStore()
    const [userRole, setUserRole] = useState(store.getState().user.userData.role)

    store.subscribe(() => {
        setUserRole(store.getState().user.userData.role)
    })

    const api = useApiContext()

    const admin = [3, 4].includes(userRole)

    const [materials, setMaterials] = useState<Material[]>()

    const [materialForDelete, setMaterialForDelete] = useState<Material | undefined>(undefined)

    const confirmDeleteMaterial= (material:Material) => {
        setMaterialForDelete(material)
    }

    const {setSuccess, setError} = useMessageContext() || {}

    const deleteMaterial = () => {
        api?.delete(`materials/${materialForDelete?.id}`).then(() => {
            setSuccess && setSuccess()
        }).catch(() => {
            setError && setError()
        }).then(() => {
            updateMaterials()
        })
    }

    const cancelDeleteMaterial = () => {
        setMaterialForDelete(undefined)
    }

    const addToArchive = (material:Material) => {
        api?.put(`materials/${material.id}`,{
            ...material,
            option:undefined,
            active:true
        }).then(() => {
            setSuccess && setSuccess()
        }).catch(() => {
            setError && setError()
        }).then(() => {
            updateMaterials()
        })
    }

    const fetchData = async () => {
        const data: {
            materials: Material[]
        } = {materials: []}

        await api?.get('materials').then((res) => {
            data.materials = res.filter((item: any) => !item.active).reverse()

            return data.materials.map(item => item.id)
        }).then((res) => {
            return Promise.all(res.map((item) => api?.get(`materials/${item}/files`))).then((files) => {
                return files.map((item, index) => {
                    return {item, id: res[index]}
                })
            })
        }).then((res) => {
            data.materials = data.materials.map((item, index) => {
                return {
                    ...item,
                    option: res[index].item.map((file:any) => {

                        return {
                            name: file.name,
                            link: `${BACKEND_URL}api/v1/files/${file.id}/download`
                        }
                    })
                }
            })
        })

        return data
    }

    const updateMaterials = () => {
        fetchData().then((data) => {
            setMaterials(data.materials)
        })
    }

    useEffect(() => {
        updateMaterials()
    },[])

    return (
        <ContentWrapper minWidth={600} maxWidth={900}>
            <article className={'materials'}>
                <header className={'materials__header'}>
                    <div className={'materials__info'}>
                        <h2 className={'materials__h2'}>
                            Архив материалов
                        </h2>
                    </div>
                    <div className={'materials__buttons'}>
                        <button className={'button'} onClick={updateMaterials}>
                            <span className={'icon'}>
                                <UpdateIcon/>
                            </span>
                        </button>
                    </div>
                </header>
                <section className={'materials__data'}>
                    {
                        materials &&
                        materials.map((material, materialIndex) =>
                            <div className={'material'} key={materialIndex}>
                                <div className={'material__header'}>
                                    <span className={'material__title'}>{material.name}</span>
                                    {
                                        admin &&
                                        <div className={'material__buttons'}>
                                            <button className={'button'} onClick={addToArchive.bind(null,material)}>Убрать из архива</button>
                                            <div className={'button'} onClick={confirmDeleteMaterial.bind(null,material)}><span
                                                className={'icon'}><Cross2Icon/></span>
                                            </div>
                                        </div>
                                    }
                                </div>
                                {
                                    material.description &&
                                    <pre className={'material__description'}>{material.description}</pre>
                                }
                                {
                                    material?.option?.length > 0 &&
                                    <div className={'material__files'}>
                                        {
                                            material?.option?.map((file, fileIndex) =>
                                                <a key={fileIndex} href={file.link} download={true}>{file.name}</a>)
                                        }
                                    </div>
                                }
                            </div>
                        )
                    }
                    {
                        materials?.length==0 &&
                        <div className={'materials__empty'}>
                            Нет доступных материалов.
                        </div>
                    }
                </section>
            </article>
            <Question trigger={materialForDelete} message={'Уверены, что хотите удалить материал'}
                      buttonMessage={'Продолжить'} action1={cancelDeleteMaterial} action2={deleteMaterial}/>
        </ContentWrapper>
    )
}

const Archive = memo(archiveFC)

export default Archive;