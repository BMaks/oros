import React, {memo, useEffect, useState} from "react";
import ContentWrapper from "../content-wrapper/content-wrapper";
import {UpdateIcon} from "@modulz/radix-icons";

import './gradebook.scss'
import {useStore} from "react-redux";
import Table, {DisplayMode, Group, Header, Student} from "../table/table";
import {useMessageContext} from "../../routes/home-page/home-page";
import {useApiContext} from "../../utils/contexts/api-context";
import {UserRole} from "../../utils/constants/permission-constants";

const gradebookFC:React.FC = () => {
    document.title = "ИО - Ведомость"

    const store = useStore()
    const [userRole, setUserRole] = useState(store.getState().user.userData.role)
    const [userID, setUserID] = useState(store.getState().user.userData.id)

    store.subscribe(() => {
        setUserRole(store.getState().user.userData.role)
        setUserID(store.getState().user.userData.id)
    })

    const {setSuccess, setError} = useMessageContext() || {}

    const assistant = [2, 3, 4].includes(userRole);

    const api = useApiContext()

    const fetchData = async () => {
        let {topHeader, groups}: { topHeader: Header[], groups: Group[] } = {
            topHeader: [],
            groups: []
        }

        await api?.get('gradebooks/headers').then((res) => {
            topHeader = res.map((item: any) => {
                    const {id: id, name: value} = item

                    return {
                        id,
                        value
                    }
                }
            )
        })

        await api?.get('gradebooks/groups').then((res) => {
            groups = res

            return res
        }).then(async (groups) => {
            return api?.get('users').then((users) => {
                return users
                    .filter((user: any) => user.role === 'STUDENT')
                    .filter((user: any) => {
                        const studentsId = groups.reduce((acc: any, group: any) => {
                            return [
                                ...acc,
                                ...group.students
                            ]
                        }, []).map((student: any) => student.id)

                        return !studentsId.includes(user.id)
                    }).map((student: any) => student.id)
            })
        }).then(async (users) => {
            await Promise.all(users.map((item: any) => {
                return api?.get(`gradebooks/student/${item}`)
            })).then((res:any) => {
                groups = [...groups, {id:'0', name:'Студенты без группы', students: res}]
            })
        })

        return {topHeader, groups}
    }

    const [topHeaders, setTopHeaders] = useState<Header[]>([])
    const [groups, setGroups] = useState<Group[]>([])

    const update = () => {
        fetchData().then(({topHeader, groups}) => {
            setTopHeaders(topHeader)
            setGroups(groups)
        })
    }

    useEffect(() => {
        update()
    }, [])

    return (
        <ContentWrapper minWidth={900} >
            <article className={'grade-book'}>
                <header className={'grade-book-header'}>
                    <div className={'grade-book__info'}>
                        <h2 className={'grade-book__h2'}>
                            {
                                assistant ?
                                    <>Общая ведомость</> :
                                    <>Моя ведомость</>
                            }
                        </h2>
                    </div>
                    <div className={'grade-book__buttons'}>
                        <button onClick={update} className={'button'}><span
                            className={'icon'}><UpdateIcon/></span>
                        </button>
                    </div>
                </header>
                <section className={'grade-book__table'}>
                    <Table topHeader={topHeaders} groups={groups} displayMode={userRole === UserRole.student? userID : DisplayMode.all}/>
                </section>
            </article>
        </ContentWrapper>
    )
}

const Gradebook = memo(gradebookFC)

export default Gradebook;