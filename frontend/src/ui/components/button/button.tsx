import React, {memo} from "react";

import './button.scss'

type Props = {
    onClick: ((..._:any) => void) | undefined,
    children: React.ReactNode,
}

const ButtonFC:React.FC<Props> = ({onClick, children}:Props) => {

    return (
        <div onClick={onClick} className={'button'}>
            {
                children
            }
        </div>
    )
}

const Button = memo(ButtonFC)

export default Button;