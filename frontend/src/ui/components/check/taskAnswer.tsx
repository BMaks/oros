import React, {memo, useEffect, useState} from "react";
import {TaskType} from "../task-creator/task-creator";

import './check.scss'
import {useApiContext} from "../../utils/contexts/api-context";
import {number} from "prop-types";
import {useStore} from "react-redux";
import {useMessageContext} from "../../routes/home-page/home-page";

type Props = {
    id:string
}

type Task = {
    name:string,
    file?: string,
    type: TaskType,
    testAns?: {
        question:string,
        questionId:string,
        answer:string,
        studentAnswer:string,
    }[],
    mark?: number,
    isChecked?:boolean
}

const taskAnswerFC:React.FC<Props> = ({id}:Props) => {
    const api = useApiContext()

    const store = useStore()
    const [userID, setUserID] = useState(store.getState().user.userData.id)
    const [userName, setUserName] = useState(store.getState().user.userData.name)
    const {setSuccess, setError} = useMessageContext() || {}
    store.subscribe(() => {
        setUserID(store.getState().user.userData.id)
        setUserName(store.getState().user.userData.name)
    })

    const dataFetch = async (id: string) => {
        const data: {
            task: any
        } = {
            task: {}
        }

        await api?.get(`solvedtasks/${id}`).then((res) => {
            data.task = {
                ...res,
                content: JSON.parse(res.content)
            }

            return res
        }).then(async (res) => {
            return api?.get(`tasks/${res.task_id}`).then((res) => {
                return res.schema_id
            })
        }).then((schema_id) => {
            return api?.get(`taskschemas/${schema_id}`).then((res) => {
                const {id, name, ...schema} = res

                data.task = {
                    ...data.task,
                    ...schema,
                    options: JSON.parse(schema.options).test.filter((item:any) => item.question)
                }

                return data.task
            })
        }).then((task) => {
            console.log(task)

            data.task.testAns = Object.keys(task.options).reduce((acc: any, item: any, index: number) => {

                return [
                    ...acc,
                    {
                        question: task.options[index].question,
                        answer: task.options[index].answer,
                        studentAnswer: task.content[index],
                        questionId: index,
                    }
                ]
            },[])
        })

        return data
    }

    const [task, setTask] = useState<Task>()

    const update = () => {
        dataFetch(id).then((data) => {
            setTask(data.task)
        })
    }

    useEffect(() => {
        update()
    },[])

    const send = (event:any) => {
        event.preventDefault()

        console.log(event.target[0].value)

        api?.get(`solvedtasks/${id}`).then((res) => {
            return res
        }).then((task) => {
            api?.post(`solvedtasks/${id}/mark`, {
                ...task,
                mark: Number.parseInt(event.target[0].value),
                description: event.target[1].value,
                taskId: task.task_id,
                inspectorId: userID
            })
        }).then(() => {
            setSuccess && setSuccess()
        }).catch(() => {
            setError && setError()
        })
    }

    return (
        <>
            <header className={'task__header'}>
                <h3 className={'task__h3'}>
                    Студент: {task?.name}
                </h3>
            </header>
            <div className={'task-data'}>
                {
                    (task?.type == TaskType.xlsx ||
                    task?.type == TaskType.python) &&
                    <div className={'task__item'}>
                        Решение: <a download={true} href={task?.file}>[Файл]</a>
                    </div>
                }
                {
                    task?.type == TaskType.test &&

                    task.testAns?.map((test, questId) =>
                        <div key={questId} className={'task__item _test _student'}>
                            <div className={'task-item__question'}>
                                <span className={'task-item__name'}>Вопрос: </span>
                                <span className={'task-item__value'}>{test.question}</span>
                            </div>
                            <div className={'task-item__answer'}>
                                <span className={'task-item__name'}>Правильный ответ: </span>
                                <span className={'task-item__value'}>{test.answer}</span>
                            </div>
                            <div className={`task-item__answer`}>
                                <span className={'task-item__name'}>Ответ студента: </span>
                                <span
                                    className={`task-item__value ${test.answer == test.studentAnswer ? '_green' : '_red'}`}>{test.studentAnswer}</span>
                            </div>
                        </div>
                    )

                }
                <form onSubmit={send} className={'task__item _check'}>
                    <div className={'task__mark'}>
                        Поставить оценку:
                        <input type={'number'} min={0} required={true} placeholder={task?.mark?.toString()} defaultValue={0} />
                    </div>
                    <div className={'task__comment'}>
                        <textarea placeholder={'Введите комментарий'} />
                    </div>
                    <div className={'task__button'}>
                        <span />
                        <input className={'button'} type={'submit'} />
                    </div>
                </form>
            </div>
        </>
    )
}

const TaskAnswer = memo(taskAnswerFC)

export default TaskAnswer;