import React, {memo, useEffect, useState} from "react";

import '../task-creator/task.scss'
import {TaskType} from "../task-creator/task-creator";
import RouteRenderer from "../../utils/route-renderer";
import {useStore} from "react-redux";
import {Link, useRouteMatch} from "react-router-dom";
import TaskAnswer from "./taskAnswer";
import TaskAnswersList from "./taskAnswersList";
import {useApiContext} from "../../utils/contexts/api-context";

type Props = {
    id:string
}

type StudentTaskType = {
    id:string,
    files?:{
        name:string,
        path:string
    }[],
    description:string,
    deadlineTime:string,
    deadlineDate:string,
    name:string,
    test?:{
        question:string,
        answer:string
    }[],
    type:TaskType,
}

const taskForCheckFC:React.FC<Props> = ({id}:Props) => {
    const store = useStore()
    const [userRole, setUserRole] = useState(store.getState().user.userData.role)
    const [userID, setUserID] = useState(store.getState().user.userData.id)

    store.subscribe(() => {
        setUserRole(store.getState().user.userData.role)
        setUserID(store.getState().user.userData.id)
    })

    let {path, url} = useRouteMatch();

    path = path.replace(/\/$/, '');
    url = url.replace(/\/$/, '');

    const [task, setTask] = useState<StudentTaskType>()
    const [answers, setAnswers] = useState<{
        name: string,
        id: string
    }[]>()

    const api = useApiContext()

    const fetchData = async () => {
        const data: {
            task: any,
            answers: any[]
        } = {
            task: {},
            answers: []
        }

        await api?.get(`tasks/${id}`).then((res) => {
            data.task = res

            return res
        }).then(async (task) => {
            return api?.get(`taskschemas/${task.schema_id}`).then((res) => {
                data.task = {
                    ...data.task,
                    ...res,
                    options: JSON.parse(res.options)
                }

                if (data.task.type === "TEST") {
                    data.task = {
                        ...data.task,
                        test: JSON.parse(res.options).test.filter((q: any) => q.question)
                    }
                }

                return task
            })
        }).then(async (task) => {
            await api?.get(`deadlines/${task.deadline_id}`).then((res) => {
                data.task = {
                    ...data.task,
                    deadlineDate: res.time
                }
            })
        }).then(async () => {
            await api?.get('solvedtasks').then((res) => {
                data.answers = res.filter((item: any) => item.task_id === id)
            })
        })

        return data
    }

    const update = () => {
        fetchData().then((data) => {
            ///TODO:ЗАПОЛНЕНО
            setTask(data.task)
            setAnswers(data.answers)
        })
    }

    useEffect(() => {
        update()
    },[])

    const TaskAnswerByID = (id:string) => <TaskAnswer id={id}/>
    const TaskAnswersListByID = (id:string) => <TaskAnswersList id={id}/>

    const answerRoutes = answers?.map(item => {
        return {
            path:item.id,
            component: TaskAnswerByID.bind(null,item.id) as React.FC
        }
    })

    answerRoutes?.push({
        path: '',
        component: TaskAnswersListByID.bind(null, id),
    })

    return (
        <div className={'task__content'}>
            <header className={'task__header'}>
                <h3 className={'task__h3'}>
                    Задание: {task?.name}
                </h3>
                <div className={'task-content__buttons'}>
                    <Link to={path + '/'} className={'button'}>К списку решений</Link>
                </div>
            </header>
            <div className={'task__data'}>
                <div className={'task__item _student'}>
                    <span className={'task-item__name'}>Описание: </span>
                    <span className={'task-item__value'}>{task?.description}</span>
                </div>
                {
                    (task?.type == TaskType.python || task?.type == TaskType.xlsx) &&
                    <>
                        <div className={'task__item  _student'}>
                            <span className={'task-item__name'}>Условия: </span>
                            {
                                task?.files?.map((file, fileIndex) =>
                                    <a key={fileIndex} download={true} href={file.path}
                                       className={'task-item__value'}>{file.name} [Файл]</a>
                                )
                            }
                        </div>
                    </>
                }
                <RouteRenderer routes={answerRoutes} path={url+'/'} />
            </div>
        </div>
    )
}

const TaskForCheck = memo(taskForCheckFC)

export default TaskForCheck;
