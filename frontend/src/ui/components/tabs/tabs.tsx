import React, {memo, useState} from "react";
import './tabs.scss'
import {ColorTheme, ZoneAlign, ZoneSettings} from "../../routes/home-page/public-page";

type Props = {
    children: React.ReactNode,
    zoneSettings?: ZoneSettings[],
    currentTabIndex?: number
}

export const TabsFC:React.FC<Props> = (
    {
        children,
        zoneSettings,
        currentTabIndex = 0,
    }: Props
) => {
    const allChildren = React.Children.map(children, (child) => child)
    const [currentTab, setCurrentTab] = useState(currentTabIndex)

    const getTheme = (theme: ColorTheme = ColorTheme.light): string => {
        switch (theme) {
            case ColorTheme.dark:
                return '_dark '
            case ColorTheme.light:
                return '_light '
            case ColorTheme.invisible:
                return '_invisible '
            default:
                return ''
        }
    }

    const getStyle = (zone: ZoneSettings): string => {
        let classNames = 'tabs__zone '

        switch (zone.align) {
            case ZoneAlign.left:
                classNames += '_left '
                break;
            case ZoneAlign.right:
                classNames += '_right '
                break;
            case ZoneAlign.center:
            default:
                classNames += '_center '
                break;
        }

        return classNames
    }

    const getZone = (zoneSettings: ZoneSettings[] | undefined = [{}]) => {
        let count = 0;

        const getChildren = (zone: ZoneSettings) => {
            const children = allChildren?.map((child, index) => {

                if (index < (zone.countOfItems || allChildren?.length))
                    return allChildren[count + index] ?
                        <div key={index} className={getTheme(zone.theme)}>{
                            React.isValidElement(allChildren[count + index]) ?
                                // @ts-ignore
                                React.cloneElement(allChildren[count + index], {
                                    setCurrentTab,
                                    tabIndex: count + index,
                                    currentTab
                                }) : null
                        }</div> : null
            })

            count += (zone.countOfItems || allChildren?.length || 0)

            return children
        }

        return zoneSettings?.map((zone, index) => {
            return (
                <div key={index} className={getStyle(zone)}>
                    {getChildren(zone)}
                </div>
            )
        })
    }

    return (
        <div className={'tabs'}>
            {
                getZone(zoneSettings)
            }
        </div>
    )
}

const Tabs = memo(TabsFC)

export default Tabs;