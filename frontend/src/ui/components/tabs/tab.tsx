import React from "react";
import Button from "../button/button";
import {Link} from "react-router-dom";

import './tab.scss'

type Props = {
    currentTab?: boolean,
    message?: string,
    link?: string,
    download?: boolean,
    mode?: TabMode,
    onClick?: (..._:any) => void,
    setCurrentTab?: (_:number) => void,
    tabIndex?:number
    href?:string,
}

export enum TabMode {
    button,
    tab,
    text,
    link,
    none
}

export const Tab:React.FC<Props> = ({currentTab, message, link, mode, onClick, setCurrentTab, tabIndex, download, href}: Props) => {
    const onTabClick = () => {
        if (mode === TabMode.tab)
            setCurrentTab && setCurrentTab(tabIndex || 0)

        onClick && onClick()
    }

    switch (mode) {
        case TabMode.button:
            return <Button onClick={onClick}>{message}</Button>
        case TabMode.link:
            return <a className={'tab tab-link'} href={href || ''} download={download}>{message}</a>
        case TabMode.tab:
            return <Link to={link || ''} onClick={onTabClick}
                         className={`tab tab-tab ${currentTab === tabIndex ? '_current-tab' : ''}`}>{message}</Link>
        case TabMode.none:
            return null
        case TabMode.text:
        default:
            return <span className={'tab tab-text'}>{message}</span>
    }
}
