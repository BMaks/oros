# OROS Backend

OROS - Operations Research Online System

Backend - REST API


## Build and Run

#### Open in IDE

configuration: OrosApplication
    
#### or

Gradle -> oros -> Tasks -> application -> bootRun

#### or run in terminal

    ./gradlew bootRun
    
    
### Run
http://localhost:8888/api/v1/

---

# Tips

    ./gradlew build -x test
    java -jar build/libs/oros-0.0.1-SNAPSHOT.jar
    
gradle : copy dir 
https://docs.gradle.org/current/userguide/working_with_files.html

docker https://docs.docker.com/get-started/08_using_compose/