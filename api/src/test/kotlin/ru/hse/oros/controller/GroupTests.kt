package ru.hse.oros.controller

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.exchange
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import ru.hse.oros.dao.entity.GroupDao
import ru.hse.oros.enums.UserRole
import ru.hse.oros.model.entity.Group
import ru.hse.oros.model.entity.User
import ru.hse.oros.model.request.entity.GroupSaveRequest
import ru.hse.oros.model.request.entity.UserSaveRequest
import ru.hse.oros.model.response.UserResponse
import ru.hse.oros.service.entity.GroupService
import ru.hse.oros.service.entity.UserService


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GroupTests(
    @Autowired val restTemplate: TestRestTemplate,
    @Autowired val service: GroupService,
    @Autowired val userService: UserService,
    @Autowired val dao: GroupDao
    ): ControllerTest() {

    private var user = User(
        token = "testGroup001",
        name = "test group user 001",
        role = UserRole.STUDENT,
        email = "testgroup_mail1@hse.ru"
                           )

    private var group = Group(
        name = "test group 001",
        events = null
                             )

    init {
        user = userService.create(
            UserSaveRequest(
                token = user.token,
                name = user.name,
                role = user.role,
                email = user.email
                           )
                                 ).copy()

        group = service.create(
            GroupSaveRequest(
                name = group.name,
                events = group.events
                            )
                              ).copy()
    }

    @Test
    fun `find groups`() {
        val request = HttpEntity<Any?>(
            HttpHeaders().apply { add("Cookie", "token=$supervisorToken") })

        with(restTemplate.exchange<List<Group>>("/groups", HttpMethod.GET, request)) {
            assertEquals(HttpStatus.OK, this.statusCode)
            assertFalse(this.body.isNullOrEmpty())
        }
    }

    @Test
    fun `create group`() {
        val request = HttpEntity<Any?>(
            mapOf("name" to "testGroup2"),
            HttpHeaders().apply { add("Cookie", "token=$supervisorToken") })

        val groupsCount = dao.findAll().count()

        with(restTemplate.exchange<String>("/groups", HttpMethod.POST, request)) {
            assertEquals(HttpStatus.CREATED, this.statusCode)
            assertTrue(dao.findAll().count() == groupsCount + 1)
        }
    }

    @Test
    fun `update group`() {
        val expectedGroup = group.copy(name = "testUpdateGroup1", events = "Seminar1")

        val request = HttpEntity<Any?>(
            mapOf("name" to expectedGroup.name, "events" to expectedGroup.events),
            HttpHeaders().apply { add("Cookie", "token=$supervisorToken") })

        val groupsCount = dao.findAll().count()

        with(restTemplate.exchange<String>("/groups/${group.id}", HttpMethod.PUT, request)) {
            assertEquals(HttpStatus.OK, this.statusCode)

            val newGroup = service.findById(group.id) as Group

            assertEquals(expectedGroup.name, newGroup.name)
            assertEquals(expectedGroup.events, newGroup.events)

            assertEquals(groupsCount, dao.findAll().count())
        }
    }

    @Test
    fun `delete group`() {
        val groupToDelete = service.create(
            GroupSaveRequest(
                name = group.name + "Delete1",
                events = null
                            )
                                          )

        val request = HttpEntity<Any?>(
            HttpHeaders().apply { add("Cookie", "token=$supervisorToken") })

        val groupsCount = dao.findAll().count()

        with(restTemplate.exchange<String>("/groups/${groupToDelete.id}", HttpMethod.DELETE, request)) {
            assertEquals(HttpStatus.OK, this.statusCode)
            assertEquals(groupsCount - 1, dao.findAll().count())
        }
    }

    @Test
    fun `add student`() {
        val request = HttpEntity<Any?>(
            HttpHeaders().apply { add("Cookie", "token=$supervisorToken") })

        with(restTemplate.exchange<String>("/groups/${group.id}/${user.id}", HttpMethod.POST, request)) {
            assertEquals(HttpStatus.OK, this.statusCode)

            val u = ((service.findByIdWithStudents(group.id) as Map<String, Any>)["students"] as List<UserResponse>)[0]
            assertEquals(user.id, u.id)
        }
    }

    @Test
    fun `delete student`() {
        val request = HttpEntity<Any?>(
            HttpHeaders().apply { add("Cookie", "token=$supervisorToken") })

        with(restTemplate.exchange<String>("/groups/${group.id}/${user.id}", HttpMethod.DELETE, request)) {
            assertEquals(HttpStatus.OK, this.statusCode)

            val studentsCount =
                ((service.findByIdWithStudents(group.id) as Map<String, Any>)["students"] as List<UserResponse>).count()
            assertEquals(0, studentsCount)
        }
    }
}