package ru.hse.oros.controller

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.exchange
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import ru.hse.oros.dao.entity.UserDao
import ru.hse.oros.enums.UserRole
import ru.hse.oros.model.entity.User
import ru.hse.oros.model.request.entity.UserSaveRequest
import ru.hse.oros.model.request.entity.UserUpdateRequest
import ru.hse.oros.service.entity.UserService

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserTests (
    @Autowired val restTemplate: TestRestTemplate,
    @Autowired val dao: UserDao,
    @Autowired val service: UserService
    ): ControllerTest() {

        private var user = User(
            token = "test001",
            name = "test user 001",
            role = UserRole.STUDENT,
            email = "test_mail1@hse.ru"
                               )

        private var user2 = User(
            token = "test002",
            name = "test user 002",
            role = UserRole.STUDENT,
            email = "test_mail2@hse.ru"
                                )

        private var user3 = User(
            token = "test003",
            name = "test user 003",
            role = UserRole.STUDENT,
            email = "test_mail3@hse.ru"
                                )

        init {
            user = dao.save(user).copy()
            user3 = dao.save(user3).copy()
        }

        @Test
        fun `create with dao`() {
            val user1 = dao.save(user2)

            Assertions.assertEquals(user2.token, user1.token)
            Assertions.assertEquals(user2.name, user1.name)
            Assertions.assertEquals(user2.role, user1.role)
            Assertions.assertEquals(user2.email, user1.email)
            Assertions.assertEquals(user2.date, user1.date)

            user2 = user2.copy()
        }

        @Test
        fun `create`() {
            val newUser = UserSaveRequest(
                token = "test001created",
                name = "test user 001created",
                role = UserRole.STUDENT,
                email = "test_mail1created@hse.ru"
                                         )

            val request = HttpEntity<Any?>(
                newUser,
                HttpHeaders().apply { add("Cookie", "token=$supervisorToken") })

            val result = restTemplate.exchange<String>("/users", HttpMethod.POST, request)


            /*ResponseEntity.ok().body(
                objectMapper.readValue(stubbedGet, responseType)
        )*/

            Assertions.assertEquals(HttpStatus.CREATED, result.statusCode)
        }

        @Test
        fun `find by id`() {
            val request = HttpEntity<Any?>(
                HttpHeaders().apply { add("Cookie", "token=$supervisorToken") })

            val result = restTemplate.exchange<User>("/users/${user.id}", HttpMethod.GET, request)

            Assertions.assertEquals(HttpStatus.OK, result.statusCode)
        }

        @Test
        fun `update`() {
            val updateBody = UserUpdateRequest( name = "testUpdatedName1", role = UserRole.ASSISTANT)

            val request = HttpEntity<Any?>(
                updateBody,
                HttpHeaders().apply { add("Cookie", "token=$supervisorToken") })

            val oldUser = user.copy()
            val expectedUser = user.copy(name = updateBody.name, role = updateBody.role)

            val result = restTemplate.exchange<String>("/users/${user.id}", HttpMethod.PUT, request)

            Assertions.assertEquals(HttpStatus.OK, result.statusCode)

            val newUser = service.findById(user.id) as User

            Assertions.assertEquals(expectedUser.token, newUser.token)
            Assertions.assertEquals(expectedUser.name, newUser.name)
            Assertions.assertEquals(expectedUser.role, newUser.role)
            Assertions.assertEquals(expectedUser.email, newUser.email)
            Assertions.assertEquals(expectedUser.date, newUser.date)

            try {
                dao.findByToken(expectedUser.token)
                Assertions.assertTrue(true)
            } catch (e: Exception) {
                Assertions.assertTrue(false)
            }

            Assertions.assertTrue(service.findByName(newUser.name).count() == 1)
            Assertions.assertTrue(service.findByName(oldUser.name).isEmpty())

            user = newUser.copy()
        }

        @Test
        fun `find all`() {
            val request = HttpEntity<Any?>(
                HttpHeaders().apply { add("Cookie", "token=$supervisorToken") })

            val result = restTemplate.exchange<List<User>>("/users", HttpMethod.GET, request)

            Assertions.assertEquals(HttpStatus.OK, result.statusCode)
        }

        @Test
        fun `delete`() {
            val request = HttpEntity<Any?>(
                HttpHeaders().apply { add("Cookie", "token=$supervisorToken") })

            val result = restTemplate.exchange<String>("/users/${user3.id}", HttpMethod.DELETE, request)

            Assertions.assertEquals(HttpStatus.OK, result.statusCode)
            Assertions.assertTrue(service.findByName(user3.name).isEmpty())
        }
}