package ru.hse.oros.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.util.*

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
class EntityNotFountException(id: UUID? = null, entity: String? = null)
    : RuntimeException("${entity?.let{"$entity "}?:""}Not found${id?.let{": id = $id"}?:""}")
