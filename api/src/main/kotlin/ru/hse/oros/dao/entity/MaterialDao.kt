package ru.hse.oros.dao.entity

import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import ru.hse.oros.model.entity.File
import ru.hse.oros.model.entity.Material
import java.util.*

@Repository
interface MaterialDao : EntityDao<Material> {
    fun findDistinctByOrderByName(): List<Material>
    fun findDistinctByOrderByDateAsc(): List<Material>

    @Query("SELECT DISTINCT m FROM Material m " +
            "INNER JOIN MaterialOwnerLink mol ON mol.material_id = m.id " +
            "" +
            "WHERE mol.owner_id IN " +
            "(SELECT g.id FROM Group g " +
            "INNER JOIN GroupStudentLink sgl ON sgl.group_id = g.id " +
            "WHERE sgl.student_id = :id) " +
            "" +
            "OR mol.owner_id = :id")
    fun findStudentMaterials(id: UUID): List<Material>

    @Query("SELECT DISTINCT f FROM File f " +
            "INNER JOIN MaterialFileLink mfl ON mfl.file_id = f.id " +
            "WHERE mfl.material_id = :id")
    fun findMaterialFiles(id: UUID): List<File>
}