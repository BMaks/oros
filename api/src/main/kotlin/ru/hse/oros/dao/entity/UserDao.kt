package ru.hse.oros.dao.entity

import org.springframework.stereotype.Repository
import ru.hse.oros.model.entity.User

@Repository
interface UserDao : EntityDao<User> {
    fun findByOrderByName(): List<User>

    fun findByToken(token: String): User?
    fun findByEmail(email: String): User?
    fun findByName(name: String): List<User>
/*
    @Query("select b from User b where b.name = :name")
    fun findByName(@Param("name") name: String?): User?*/

}