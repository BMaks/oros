package ru.hse.oros.dao.entity

import org.springframework.stereotype.Repository
import ru.hse.oros.model.entity.File
import java.util.*

@Repository
interface FileDao : EntityDao<File> {
    fun findByOrderByName(): List<File>
    fun findByOrderByDateAsc(): List<File>

/*    @Query("SELECT m FROM File m " +
            "INNER JOIN FileOwnerLink mol ON mol.File_id = m.id " +
            "" +
            "WHERE mol.owner_id IN " +
            "(SELECT g.id FROM Group g " +
            "INNER JOIN StudentGroupLink sgl ON sgl.group_id = g.id " +
            "WHERE sgl.student_id = :id) " +
            "" +
            "OR mol.owner_id = :id")*/
//    fun findStudentFiles(id: UUID): List<File> // todo file
}