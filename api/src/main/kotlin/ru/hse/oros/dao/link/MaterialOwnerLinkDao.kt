package ru.hse.oros.dao.link

import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import ru.hse.oros.model.entity.link.MaterialOwnerLink
import ru.hse.oros.model.entity.link.MaterialOwnerLinkId
import java.util.*

@Repository
interface MaterialOwnerLinkDao : LinkDao<MaterialOwnerLink, MaterialOwnerLinkId> {
    @Transactional
    @Modifying
    @Query("DELETE FROM MaterialOwnerLink l WHERE l.material_id = :entity1Id AND l.owner_id = :entity2Id")
    override fun delete(entity1Id: UUID, entity2Id: UUID)

    @Transactional
    @Modifying
    @Query("DELETE FROM MaterialOwnerLink l WHERE l.material_id = :entity1Id AND l.owner_id IN :links")
    override fun delete(entity1Id: UUID, @Param("links") links: List<UUID>)

    @Transactional
    @Modifying
    @Query("DELETE FROM MaterialOwnerLink l WHERE l.material_id = :id")
    override fun deleteEntity1(id: UUID)

    @Transactional
    @Modifying
    @Query("DELETE FROM MaterialOwnerLink l WHERE l.owner_id = :id")
    override fun deleteEntity2(id: UUID)
}