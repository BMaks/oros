package ru.hse.oros.dao.entity

import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import ru.hse.oros.model.entity.Notification
import ru.hse.oros.model.entity.Task
import ru.hse.oros.model.entity.link.NotificationOwnerLink
import java.util.*

@Repository
interface NotificationDao : EntityDao<Notification> {
    fun findDistinctByOrderByDateAsc(): List<Notification>

    @Query("SELECT DISTINCT n FROM Notification n WHERE n.type = 'news' ORDER BY n.date ASC")
    fun findNewsByOrderByDate(): List<Notification>

    @Query("SELECT COUNT(DISTINCT n) FROM Notification n " +
            "INNER JOIN NotificationOwnerLink nol ON n.id = nol.notification_id " +
            "WHERE nol.owner_id = :id " +
            "AND nol.is_viewed = false")
    fun countUnviewed(id: UUID): Int

    @Query("SELECT DISTINCT nol FROM NotificationOwnerLink nol WHERE nol.owner_id = :studentId")
    fun findStudentNotificationLinks(studentId: UUID): List<NotificationOwnerLink>

    @Query("SELECT DISTINCT (n), nol.is_viewed FROM Notification n " +
            "INNER JOIN NotificationOwnerLink nol ON n.id = nol.notification_id " +
            "WHERE nol.owner_id = :studentId")
    fun findStudentNotifications(studentId: UUID): List<List<Any?>>

    @Transactional
    @Modifying
    @Query("DELETE FROM Notification n WHERE n.id = :id")
    fun delete(id: UUID)

    @Transactional
    @Modifying
    @Query("DELETE FROM Notification n WHERE n.id IN :ids")
    fun delete(@Param("ids") ids: List<UUID>)

/*
    @Query("SELECT DISTINCT n. FROM Notification n " +
            "INNER JOIN TaskOwnerLink tol ON tol.task_id = t.id " +
            "WHERE tol.owner_id IN :ids " +
            "OR tol.owner_id IN " +
            "(SELECT DISTINCT gsl.student_id FROM GroupStudentLink gsl " +
            "WHERE gsl.group_id IN :ids)")
    fun findOwnerNews(@Param("ids") ids: List<UUID>): List<List<Any?>>*/
}
