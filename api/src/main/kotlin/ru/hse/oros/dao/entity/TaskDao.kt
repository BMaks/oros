package ru.hse.oros.dao.entity

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import ru.hse.oros.model.entity.Deadline
import ru.hse.oros.model.entity.Task
import java.util.*

@Repository
interface TaskDao : EntityDao<Task> {
    fun findDistinctByOrderByDateAsc(): List<Task>

    @Query("SELECT DISTINCT t FROM Task t " +
            "INNER JOIN TaskOwnerLink tol ON tol.task_id = t.id " +
            "WHERE tol.owner_id = :groupId " +
            "ORDER BY t.date ASC")
    fun findGroupTasks(groupId: UUID): List<Task>

    @Query("SELECT DISTINCT t FROM Task t " +
            "INNER JOIN TaskOwnerLink tol ON tol.task_id = t.id " +
            "WHERE tol.owner_id IN :ids " +
            "OR tol.owner_id IN " +
            "(SELECT DISTINCT gsl.student_id FROM GroupStudentLink gsl " +
            "WHERE gsl.group_id IN :ids) " +
            "ORDER BY t.date ASC")
    fun findOwnerTasks(@Param("ids") ids: List<UUID>): List<Task>

    @Query("SELECT DISTINCT t FROM Task t " +
            "INNER JOIN TaskOwnerLink tol ON tol.task_id = t.id " +
            "WHERE tol.owner_id = :id " +
            "OR tol.owner_id IN " +
            "(SELECT gsl.group_id FROM GroupStudentLink gsl " +
            "WHERE gsl.student_id = :id) " +
            "ORDER BY t.date ASC")
    fun findStudentTasks(id: UUID): List<Task>
}