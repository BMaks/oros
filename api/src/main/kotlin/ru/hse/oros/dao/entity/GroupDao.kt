package ru.hse.oros.dao.entity

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import ru.hse.oros.model.entity.Group
import ru.hse.oros.model.entity.User
import java.util.*

@Repository
interface GroupDao : EntityDao<Group> {
    fun findDistinctByOrderByName(): List<Group>
    fun findDistinctByOrderByDateAsc(): List<Group>

    @Query("SELECT DISTINCT g FROM Group g " +
            "INNER JOIN GroupStudentLink l ON l.group_id = g.id " +
            "WHERE l.student_id = :studentId " +
            "ORDER BY g.name")
    fun findStudentGroupByOrderByName(studentId: UUID): List<Group>

    @Query("SELECT DISTINCT l.student_id FROM GroupStudentLink l WHERE l.group_id = :groupId")
    fun getGroupStudentIds(groupId: UUID): List<UUID>

    @Query("SELECT DISTINCT u FROM User u WHERE u.id IN " +
            "(SELECT DISTINCT l.student_id FROM GroupStudentLink l WHERE l.group_id = :groupId)")
    fun getGroupStudents(groupId: UUID): List<User>

/*
    @Query("SELECT u.id, u.name FROM User u")
    fun getA(): List<List<Any?>>
*/
}