package ru.hse.oros.dao.link

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.NoRepositoryBean
import org.springframework.data.repository.query.Param
import ru.hse.oros.model.entity.AbstractEntity
import ru.hse.oros.model.entity.link.Link
import ru.hse.oros.model.entity.link.LinkId
import java.util.*

@NoRepositoryBean
interface LinkDao<T : Link, ID: LinkId> : JpaRepository<T, ID> {
    fun delete(entity1Id: UUID, entity2Id: UUID)
    fun delete(entity1Id: UUID, @Param("links") links: List<UUID>)
    fun deleteEntity1(id: UUID)
    fun deleteEntity2(id: UUID)
}
