package ru.hse.oros.dao.link

import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import ru.hse.oros.model.entity.link.MaterialFileLink
import ru.hse.oros.model.entity.link.MaterialFileLinkId
import java.util.*

@Repository
interface MaterialFileLinkDao : LinkDao<MaterialFileLink, MaterialFileLinkId> {
    @Transactional
    @Modifying
    @Query("DELETE FROM MaterialFileLink l WHERE l.material_id = :entity1Id AND l.file_id = :entity2Id")
    override fun delete(entity1Id: UUID, entity2Id: UUID)
        
    @Transactional
    @Modifying
    @Query("DELETE FROM MaterialFileLink l WHERE l.material_id = :entity1Id AND l.file_id IN :links")
    override fun delete(entity1Id: UUID, @Param("links") links: List<UUID>)
    
    @Transactional
    @Modifying
    @Query("DELETE FROM MaterialFileLink l WHERE l.material_id = :id")
    override fun deleteEntity1(id: UUID)

    @Transactional
    @Modifying
    @Query("DELETE FROM MaterialFileLink l WHERE l.file_id = :id")
    override fun deleteEntity2(id: UUID)
}