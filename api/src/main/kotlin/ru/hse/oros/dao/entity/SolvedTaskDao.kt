package ru.hse.oros.dao.entity

import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import ru.hse.oros.model.entity.Deadline
import ru.hse.oros.model.entity.SolvedTask
import java.util.*

@Repository
interface SolvedTaskDao : EntityDao<SolvedTask> {
    fun findDistinctByOrderByDateAsc(): List<SolvedTask>

    @Transactional
    @Modifying
    @Query("DELETE FROM SolvedTask t WHERE t.student_id = :id")
    fun deleteStudentTasks(id: UUID)

    @Query("SELECT t FROM SolvedTask t WHERE t.student_id = :studentId")
    fun findStudentSolvedTasks(studentId: UUID): List<SolvedTask>
}