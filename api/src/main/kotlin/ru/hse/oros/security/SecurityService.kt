package ru.hse.oros.security

import org.slf4j.Logger
import ru.hse.oros.enums.AccessType
import ru.hse.oros.enums.UserRole
import ru.hse.oros.model.entity.User
import java.util.*
import javax.servlet.http.HttpServletRequest


interface SecurityService {
    val serviceName: String
    val logger: Logger

    fun hasAccess(token: String, accessType: AccessType): Boolean
    fun hasAccess(role: UserRole, accessType: AccessType): Boolean
    fun validateUser(token: String, accessType: AccessType)
    fun validateToken(token: String): User
    fun validateAndGetUser(token: String, accessType: AccessType): User
    fun authenticate(httpRequest: HttpServletRequest, user: User)
    fun run(httpRequest: HttpServletRequest, token: String, accessType: AccessType)
    fun getClient(): User
    fun isOROS(id: UUID): Boolean
    fun isOROS(role: UserRole): Boolean
}