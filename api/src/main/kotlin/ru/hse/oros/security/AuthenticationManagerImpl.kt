package ru.hse.oros.security

import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.AuthenticationServiceException
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Service

@Service
class AuthenticationManagerImpl: AuthenticationManager {
    override fun authenticate(authentication: Authentication): Authentication {
        return authentication
    }
}