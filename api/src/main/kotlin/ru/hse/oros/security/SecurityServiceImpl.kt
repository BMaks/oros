package ru.hse.oros.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.context.HttpSessionSecurityContextRepository
import org.springframework.stereotype.Service
import ru.hse.oros.enums.AccessType
import ru.hse.oros.enums.UserRole
import ru.hse.oros.exception.AccessDeniedException
import ru.hse.oros.exception.InvalidTokenException
import ru.hse.oros.logger.logger
import ru.hse.oros.model.entity.User
import ru.hse.oros.service.entity.UserService
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpSession

@Service
class SecurityServiceImpl(
    @Autowired private val authManager: AuthenticationManagerImpl
                         ) : SecurityService {

    @Autowired private lateinit var userService: UserService

    override val serviceName: String
        get() = "SecurityService"

    override val logger
        get() = logger(this)


    override fun hasAccess(token: String, accessType: AccessType): Boolean {
        val user = userService.findByToken(token) ?: throw InvalidTokenException()
        return hasAccess(user.role, accessType)
    }

    override fun hasAccess(role: UserRole, accessType: AccessType): Boolean {
        return role == UserRole.SUPERVISOR || when(accessType) {
            AccessType.ALL -> true
            AccessType.OROS ->
                when(role) {
                    UserRole.TEACHER,
                    UserRole.ASSISTANT -> true
                    else -> false
                }
            AccessType.TEACHERS   -> role == UserRole.TEACHER
            AccessType.ASSISTANTS -> role == UserRole.ASSISTANT
            AccessType.STUDENTS   -> role == UserRole.STUDENT
            else -> false
        }
    }

    override fun validateUser(token: String, accessType: AccessType) {
        if(!hasAccess(token, accessType)) throw AccessDeniedException(accessType.name)
    }

    override fun validateAndGetUser(token: String, accessType: AccessType): User {
        val user = userService.findByToken(token) ?: throw InvalidTokenException()
        if(!hasAccess(user.role, accessType)) throw AccessDeniedException(user.role.name)
        return user
    }

    override fun validateToken(token: String): User {
        return userService.findByToken(token) ?: throw InvalidTokenException()
    }

    override fun authenticate(httpRequest: HttpServletRequest, user: User) {
        val authReq = UsernamePasswordAuthenticationToken(user, null)
        val auth = authManager.authenticate(authReq)
        val sc = SecurityContextHolder.getContext()
        sc.authentication = auth

        val session: HttpSession = httpRequest.getSession(true)
        session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, sc)
    }

    override fun run(httpRequest: HttpServletRequest, token: String, accessType: AccessType) {
        val user = validateAndGetUser(token, accessType)
        authenticate(httpRequest, user)
    }

    override fun getClient(): User {
        return SecurityContextHolder.getContext().authentication.principal as User
    }

    override fun isOROS(id: UUID): Boolean {
        val user = userService.findById(id) as User
        return isOROS(user.role)
    }

    override fun isOROS(role: UserRole): Boolean {
        return when(role) {
            UserRole.SUPERVISOR,
            UserRole.TEACHER,
            UserRole.ASSISTANT -> true
            else -> false
        }
    }


}