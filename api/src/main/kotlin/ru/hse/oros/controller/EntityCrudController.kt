package ru.hse.oros.controller

import org.springframework.http.ResponseEntity
import ru.hse.oros.model.entity.AbstractEntity
import ru.hse.oros.enums.AccessType
import ru.hse.oros.model.request.entity.EntityRequest
import java.util.*
import javax.servlet.http.HttpServletRequest

interface  EntityCrudController<T: AbstractEntity> {
    fun findAll(token: String, httpRequest: HttpServletRequest, accessType: AccessType): List<Any>

    fun findById(token: String, httpRequest: HttpServletRequest, accessType: AccessType, id: UUID): ResponseEntity<Any>

    fun create(token: String, httpRequest: HttpServletRequest, accessType: AccessType, request: EntityRequest): ResponseEntity<Any>

    fun update(token: String, httpRequest: HttpServletRequest, accessType: AccessType, id: UUID, request: EntityRequest): ResponseEntity<Any>

    fun delete(token: String, httpRequest: HttpServletRequest, accessType: AccessType, id: UUID): ResponseEntity<Any>

}