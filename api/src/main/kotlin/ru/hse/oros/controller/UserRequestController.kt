package ru.hse.oros.controller

import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import ru.hse.oros.enums.AccessType
import ru.hse.oros.logger.logger
import ru.hse.oros.model.entity.UserRequest
import ru.hse.oros.model.request.entity.UserRequestRegisterRequest
import ru.hse.oros.model.request.entity.UserRequestSaveRequest
import ru.hse.oros.model.request.entity.UserRequestUpdateRequest
import ru.hse.oros.service.entity.UserRequestService
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid


@Validated
@RestController
@RequestMapping("/requests", produces = [MediaType.APPLICATION_JSON_VALUE])
class UserRequestController(
        @Autowired override val service: UserRequestService
                           ) : BaseEntityController<UserRequest>() {

    override val logger: Logger
        get() = logger(this)


    @GetMapping
    fun findAll(@CookieValue token: String,
                httpRequest: HttpServletRequest
               ): List<Any> {
        return super.findAll(token, httpRequest, AccessType.ALL)
    }

    @GetMapping("/{id}")
    fun findById(@CookieValue token: String,
                 @PathVariable id: UUID,
                 httpRequest: HttpServletRequest
                ): ResponseEntity<Any> {
        return super.findById(token, httpRequest, AccessType.ALL, id)
    }

    @PostMapping
    fun create(@CookieValue token: String,
               @Valid @RequestBody request: UserRequestSaveRequest,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.create(token, httpRequest, AccessType.ALL, request)
    }

    @PostMapping("/submit/{id}")
    fun submit(@CookieValue token: String,
               @PathVariable id: UUID,
               httpRequest: HttpServletRequest
              ) {
        security.run(httpRequest, token, AccessType.OROS)
        service.submit(id)
    }

    @PostMapping("/user")
    fun requestNewUser(@Valid @RequestBody request: UserRequestRegisterRequest) {
        service.requestNewUser(request)
    }

    @PutMapping("/{id}")
    fun update(@CookieValue token: String,
               @PathVariable id: UUID,
               @Valid @RequestBody request: UserRequestUpdateRequest,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.update(token, httpRequest, AccessType.ALL, id, request)
    }

    @DeleteMapping("/{id}")
    fun delete(
        @CookieValue token: String,
        @PathVariable id: UUID,
        httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.delete(token, httpRequest, AccessType.OROS, id)
    }

}


