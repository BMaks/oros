package ru.hse.oros.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import ru.hse.oros.enums.AccessType
import ru.hse.oros.security.SecurityService
import ru.hse.oros.service.GradebookService
import java.util.*
import javax.servlet.http.HttpServletRequest


@Validated
@RestController
@RequestMapping("/gradebooks", produces = [MediaType.APPLICATION_JSON_VALUE])
class GradebookController(
        @Autowired val service: GradebookService,
        @Autowired val security: SecurityService
                         ) {

    @GetMapping("/headers")
    fun getHeaders(@CookieValue token: String,
                   httpRequest: HttpServletRequest
                  ): List<Any> {
        security.run(httpRequest, token, AccessType.ALL)
        return service.getHeaders()
    }

    @GetMapping("/groups")
    fun getGroups(@CookieValue token: String,
                  httpRequest: HttpServletRequest
                 ): List<Any> {
        security.run(httpRequest, token, AccessType.ALL)
        return service.getGroups()
    }

    @GetMapping("/student/{id}", "/student")
    fun getStudent(@CookieValue token: String,
                   @PathVariable(required = false) id: UUID?,
                   httpRequest: HttpServletRequest
                 ): Map<String, Any?> {
        security.run(httpRequest, token, AccessType.ALL)
        return service.getStudent(id)
    }

    @GetMapping
    fun getGradeBooks(@CookieValue token: String,
                      httpRequest: HttpServletRequest
                     ): Map<String, Any?> {
        security.run(httpRequest, token, AccessType.ALL)
        return service.getGradeBooks()
    }

    @GetMapping("/{id}/download", produces = [MediaType.APPLICATION_OCTET_STREAM_VALUE])
    fun downloadFile(
        @CookieValue token: String,
        @PathVariable id: UUID,
        httpRequest: HttpServletRequest
                    ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.ALL)
        return ResponseEntity.ok(service.createReport(id))
    }
}


