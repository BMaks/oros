package ru.hse.oros.controller

import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import ru.hse.oros.model.entity.Task
import ru.hse.oros.enums.AccessType
import ru.hse.oros.logger.logger
import ru.hse.oros.model.request.entity.TaskSaveRequest
import ru.hse.oros.model.request.entity.TaskUpdateRequest
import ru.hse.oros.service.entity.TaskService
import ru.hse.oros.service.link.TaskOwnerLinkService
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid


@Validated
@RestController
@RequestMapping("/tasks", produces = [MediaType.APPLICATION_JSON_VALUE])
class TaskController(
        @Autowired override val service: TaskService,
        @Autowired val taskOwnerLinkService: TaskOwnerLinkService
                    ) : BaseEntityController<Task>() {

    override val logger: Logger
        get() = logger(this)


    @GetMapping
    fun findAll(
        @CookieValue token: String,
        httpRequest: HttpServletRequest
               ): List<Any> {
        return super.findAll(token, httpRequest, AccessType.ALL)
    }

    @GetMapping("/{id}")
    fun findById(
        @CookieValue token: String,
        @PathVariable id: UUID,
        httpRequest: HttpServletRequest
                ): ResponseEntity<Any> {
        return super.findById(token, httpRequest, AccessType.ALL, id)
    }

    @PostMapping
    fun create(
        @CookieValue token: String,
        @Valid @RequestBody request: TaskSaveRequest,
        httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.create(token, httpRequest, AccessType.OROS, request)
    }

    @PutMapping("/{id}")
    fun update(
        @CookieValue token: String,
        @PathVariable id: UUID,
        @Valid @RequestBody request: TaskUpdateRequest,
        httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.update(token, httpRequest, AccessType.OROS, id, request)
    }

    @DeleteMapping("/{id}")
    fun delete(
        @CookieValue token: String,
        @PathVariable id: UUID,
        httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.delete(token, httpRequest, AccessType.OROS, id)
    }

    @PostMapping("/{id}/students")
    fun createTaskOwnerLinks(
        @CookieValue token: String,
        @PathVariable id: UUID,
        @Valid @RequestBody request: List<UUID>, // students and groups
        httpRequest: HttpServletRequest
                         ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.OROS)

        taskOwnerLinkService.create(id, request)
        return ResponseEntity(HttpStatus.CREATED)
    }

    @PutMapping("/{id}/students")
    fun setTaskOwnerLinks(
        @CookieValue token: String,
        @PathVariable id: UUID,
        @Valid @RequestBody request: List<UUID>, // students and groups
        httpRequest: HttpServletRequest
                             ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.OROS)

        taskOwnerLinkService.set(id, request)
        return ResponseEntity(HttpStatus.OK)
    }

    @DeleteMapping("/{id}/students")
    fun deleteTaskOwnerLinks(
        @CookieValue token: String,
        @PathVariable id: UUID,
        @Valid @RequestBody request: List<UUID>, // students and groups
        httpRequest: HttpServletRequest
                            ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.OROS)

        taskOwnerLinkService.delete(id, request)
        return ResponseEntity(HttpStatus.OK)
    }
}


