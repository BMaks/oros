package ru.hse.oros.controller

import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import ru.hse.oros.enums.AccessType
import ru.hse.oros.logger.logger
import ru.hse.oros.model.entity.Group
import ru.hse.oros.model.request.entity.GroupSaveRequest
import ru.hse.oros.service.entity.GroupService
import ru.hse.oros.service.link.GroupStudentLinkService
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid


@Validated
@RestController
@RequestMapping("/groups", produces = [MediaType.APPLICATION_JSON_VALUE])
class GroupController(
    @Autowired override val service: GroupService,
    @Autowired private val groupStudentLinkService: GroupStudentLinkService
                     ) : BaseEntityController<Group>() {

    override val logger: Logger
        get() = logger(this)


    @GetMapping
    fun findAll(@CookieValue token: String,
                @RequestParam(required = false) showStudents: Boolean = false,
                @RequestParam(required = false) showAll: Boolean = false,
                httpRequest: HttpServletRequest
               ): List<Any> {
        security.run(httpRequest, token, AccessType.ALL)
        val params = mapOf("showStudents" to showStudents, "showAll" to showAll)
        return service.findAll(params)
    }

    @PostMapping("/{groupId}/{studentId}")
    fun addStudent(@CookieValue token: String,
                   @PathVariable groupId: UUID,
                   @PathVariable studentId: UUID,
                   httpRequest: HttpServletRequest
                  ) {
        security.run(httpRequest, token, AccessType.OROS)

        return service.addStudent(groupId, studentId)
    }

    @DeleteMapping("/{groupId}/{studentId}")
    fun deleteStudent(@CookieValue token: String,
                      @PathVariable groupId: UUID,
                      @PathVariable studentId: UUID,
                      httpRequest: HttpServletRequest
                     ) {
        security.run(httpRequest, token, AccessType.OROS)

        return service.deleteStudent(groupId, studentId)
    }

    @GetMapping("/{id}")
    fun findById(@CookieValue token: String,
                 @PathVariable id: UUID,
                 @RequestParam(required = false) withStudents: Boolean = false,
                 httpRequest: HttpServletRequest
                ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.ALL)

        return if (withStudents)
            ResponseEntity.ok(service.findByIdWithStudents(id))
        else
            ResponseEntity.ok(service.findById(id))
    }

    @PostMapping
    fun create(@CookieValue token: String,
               @Valid @RequestBody request: GroupSaveRequest,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.create(token, httpRequest, AccessType.OROS, request)
    }

    @PutMapping("/{id}")
    fun update(@CookieValue token: String,
               @PathVariable id: UUID,
               @Valid @RequestBody request: GroupSaveRequest,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.update(token, httpRequest, AccessType.OROS, id, request)
    }

    @DeleteMapping("/{id}")
    fun delete(@CookieValue token: String,
               @PathVariable id: UUID,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.delete(token, httpRequest, AccessType.OROS, id)
    }

    @PostMapping("/{id}/students")
    fun createGroupStudentLinks(
        @CookieValue token: String,
        @PathVariable id: UUID,
        @Valid @RequestBody request: List<UUID>, // students and groups
        httpRequest: HttpServletRequest
                                ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.OROS)

        groupStudentLinkService.create(id, request)
        return ResponseEntity(HttpStatus.CREATED)
    }


    @PutMapping("/{id}/students")
    fun setGroupStudentLinks(
        @CookieValue token: String,
        @PathVariable id: UUID,
        @Valid @RequestBody request: List<UUID>, // students and groups
        httpRequest: HttpServletRequest
                             ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.OROS)

        groupStudentLinkService.set(id, request)
        return ResponseEntity(HttpStatus.OK)
    }

    @DeleteMapping("/{id}/students")
    fun deleteGroupStudentLinks(
        @CookieValue token: String,
        @PathVariable id: UUID,
        @Valid @RequestBody request: List<UUID>, // students and groups
        httpRequest: HttpServletRequest
                                ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.OROS)

        groupStudentLinkService.delete(id, request)
        return ResponseEntity(HttpStatus.OK)
    }
}


