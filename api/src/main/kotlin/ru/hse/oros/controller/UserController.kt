package ru.hse.oros.controller

import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import ru.hse.oros.model.entity.User
import ru.hse.oros.enums.AccessType
import ru.hse.oros.logger.logger
import ru.hse.oros.model.request.entity.FileMaterialSaveRequest
import ru.hse.oros.model.request.entity.UserSaveRequest
import ru.hse.oros.model.request.entity.UserUpdateRequest
import ru.hse.oros.service.entity.UserService
import ru.hse.oros.utils.toUserResponse
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid


@Validated
@RestController
@RequestMapping("/users", produces = [MediaType.APPLICATION_JSON_VALUE])
class  UserController(
        @Autowired override val service: UserService
                     ): BaseEntityController<User>() {

    override val logger: Logger
        get() = logger(this)


    @GetMapping
    fun findAll(@CookieValue token: String,
                httpRequest: HttpServletRequest
               ): List<Any> {
        security.run(httpRequest, token, AccessType.ALL)

        return service.findAllToResponse()
    }

    @GetMapping("/client")
    fun find(@CookieValue token: String,
                 httpRequest: HttpServletRequest
                ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.ALL)

        return ResponseEntity.ok(service.findByTokenToResponse(token))
    }

    @GetMapping("/{id}")
    fun findById(@CookieValue token: String,
                 @PathVariable id: UUID,
                 httpRequest: HttpServletRequest
                ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.ALL)

        return ResponseEntity.ok(service.findByIdToResponse(id))
    }

    @PostMapping
    fun create(@CookieValue token: String,
               @Valid @RequestBody request: UserSaveRequest,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.create(token, httpRequest, AccessType.OROS, request)
    }

    @PutMapping("/{id}")
    fun update(@CookieValue("token") token: String,
               @PathVariable id: UUID,
               @Valid @RequestBody request: UserUpdateRequest,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.update(token, httpRequest, AccessType.OROS, id, request)
    }

    @DeleteMapping("/{id}")
    fun delete(
            @CookieValue token: String,
            @PathVariable id: UUID,
            httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.delete(token, httpRequest, AccessType.OROS, id)
    }

    }
/*[{
    author: 'Иванов Иван',
    authorID: '234524523 3452345sdfsdf 2345 2345',
    msg: 'stringannnnnnnnnasdnn\n\n\nsdasdasdasd aasdas asd sa asd',
    date: '25.14.1263 16:76',
    tags: ['Важно', 'Прочее'],
    id: '2',
    isViewed: false,
    source: '/app/news'
}]*/


