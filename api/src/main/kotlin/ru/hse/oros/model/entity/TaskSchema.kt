package ru.hse.oros.model.entity

import ru.hse.oros.enums.TaskCategoryType
import ru.hse.oros.enums.TaskType
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name="task_schema")
data class TaskSchema(
    @Id
    @GeneratedValue
    override val id: UUID = UUID.randomUUID(),
    override val date: Date = Date(),

    val name: String,
    val description: String? = null,
    val type: TaskType,
    val category: TaskCategoryType,
    val options: String? = null
                     ) : AbstractEntity()
