package ru.hse.oros.model.request.entity

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull
import ru.hse.oros.model.request.entity.EntityRequest
import java.util.*

data class NotificationSaveRequest(
    @JsonProperty("author")
    @get:NotNull
    val author: String,

    @JsonProperty("tags")
    val tags: String? = null,

    @JsonProperty("msg")
    val msg: String? = null,

    @JsonProperty("source")
    val source: String? = null,

    @JsonProperty("links")
    val links: List<UUID>? = null
                                  ) : EntityRequest