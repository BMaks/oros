package ru.hse.oros.model.entity

import ru.hse.oros.enums.UserRole
import java.util.*
import javax.persistence.*

@Entity
@Table
data class User (
    @Id
    @GeneratedValue
    override val id: UUID = UUID.randomUUID(),
    override val date: Date = Date(),

    val token: String,
    val name: String,
    val role: UserRole,
    val email: String

                ) : AbstractEntity()






/*@JsonDeserialize(keyUsing = MoviesMapKeyDeserializer::class)
@JsonSerialize(keyUsing = MoviesMapKeySerializer::class)
val map : Map<*,*>*/


//        @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id") // не обязательно
//        @Id
//        val id: UUID = UUID.randomUUID(),

//        @Column(name = "created") // не обязательно
