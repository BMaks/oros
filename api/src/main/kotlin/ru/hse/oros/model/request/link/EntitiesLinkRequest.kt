package ru.hse.oros.model.request.link

import java.util.*

interface EntitiesLinkRequest: LinkRequest {
    val entity1_id: UUID
    val entity2_id: UUID
}