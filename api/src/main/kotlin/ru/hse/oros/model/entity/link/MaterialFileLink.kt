package ru.hse.oros.model.entity.link

import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "material_file_link")
@IdClass(MaterialFileLinkId::class)
data class MaterialFileLink(
    @Id
    val material_id: UUID,
    @Id
    val file_id: UUID
                           ): Link


@Embeddable
data class MaterialFileLinkId(
    val material_id: UUID,
    val file_id: UUID
                        ): Serializable, LinkId
