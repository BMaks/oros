package ru.hse.oros.model.request.link

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull
import java.util.*

data class MaterialFileLinkRequest(
    @JsonProperty("materialId")
    @get:NotNull
    val material_id: UUID,
    override val entity1_id: UUID = material_id,

    @JsonProperty("fileId")
    @get:NotNull
    val file_id: UUID,
    override val entity2_id: UUID = file_id,

    ) : EntitiesLinkRequest