package ru.hse.oros.model.entity.link

import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "material_owner_link")
@IdClass(MaterialOwnerLinkId::class)
data class MaterialOwnerLink(
    @Id
    val material_id: UUID,
    @Id
    val owner_id: UUID
                           ): Link


@Embeddable
data class MaterialOwnerLinkId(
    val material_id: UUID,
    val owner_id: UUID
                        ): Serializable, LinkId
