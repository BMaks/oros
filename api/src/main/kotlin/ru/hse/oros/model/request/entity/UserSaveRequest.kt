package ru.hse.oros.model.request.entity

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull
import ru.hse.oros.enums.UserRole

data class UserSaveRequest(

//        @get:NotNull
//        val id: UUID,

//        @get:Size(min = 1, max = 50)
        @JsonProperty("token")
        @get:NotNull
        val token: String,

        @JsonProperty("name")
        @get:NotNull
        val name: String,

        @JsonProperty("role")
        @get:NotNull
        val role: UserRole,

        @JsonProperty("email")
        @get:NotNull
        val email: String

//        @get:Positive

//        @get:NotNull
//        @get:Past
//        val created: LocalDate?
                          ) : EntityRequest
