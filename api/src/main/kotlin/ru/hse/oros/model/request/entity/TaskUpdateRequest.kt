package ru.hse.oros.model.request.entity

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class TaskUpdateRequest(

    @JsonProperty("deadlineId")
    val deadline_id: UUID? = null
                            ) : EntityRequest