package ru.hse.oros.model.request.entity

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class SolvedTaskUpdateRequest(

    @JsonProperty("description")
    val description: String? = null,

    @JsonProperty("content")
    val content: String? = null

                                  ) : EntityRequest