package ru.hse.oros.model.request.entity

import com.fasterxml.jackson.annotation.JsonProperty

data class UserRequestUpdateRequest(
        @JsonProperty("source")
        val source: String? = null
                                   ) : EntityRequest
