package ru.hse.oros.model.entity

import ru.hse.oros.enums.UserRequestType
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name="request")
data class UserRequest(
    @Id
    @GeneratedValue
    override val id: UUID = UUID.randomUUID(),
    override val date: Date = Date(),

    val user_id: UUID? = null,
    val type: UserRequestType,
    val source: String? = null
                  ) : AbstractEntity()
