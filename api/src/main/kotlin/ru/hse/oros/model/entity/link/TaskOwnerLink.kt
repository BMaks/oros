package ru.hse.oros.model.entity.link

import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "task_owner_link")
@IdClass(TaskOwnerLinkId::class)
data class TaskOwnerLink(
    @Id
    val task_id: UUID,
    @Id
    val owner_id: UUID
                           ): Link


@Embeddable
data class TaskOwnerLinkId(
    val task_id: UUID,
    val owner_id: UUID
                        ): Serializable, LinkId
