package ru.hse.oros.model.request.entity

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull
import ru.hse.oros.enums.UserRole

data class UserUpdateRequest(

        @JsonProperty("name")
        @get:NotNull
        val name: String,

        @JsonProperty("role")
        @get:NotNull
        val role: UserRole

                            ) : EntityRequest
