package ru.hse.oros.model.request.entity

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull
import java.util.*

data class TaskSaveRequest(
    @JsonProperty("schemaId")
    @get:NotNull
    val schema_id: UUID,

    @JsonProperty("name")
    @get:NotNull
    val name: String,

    @JsonProperty("deadlineId")
    val deadline_id: UUID? = null,

    @JsonProperty("links")
    val links: List<UUID>? = null
                          ) : EntityRequest