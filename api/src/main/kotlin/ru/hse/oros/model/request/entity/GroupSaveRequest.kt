package ru.hse.oros.model.request.entity

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull
import java.util.*

data class GroupSaveRequest(

    @JsonProperty("name")
    @get:NotNull
    val name: String,

    @JsonProperty("events")
    val events: String? = null,

    @JsonProperty("links")
    val links: List<UUID>? = null

    ) : EntityRequest