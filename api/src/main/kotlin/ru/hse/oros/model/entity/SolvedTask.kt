package ru.hse.oros.model.entity

import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name="solved_task")
data class SolvedTask(
    @Id
    @GeneratedValue
    override val id: UUID = UUID.randomUUID(),
    override val date: Date = Date(),

    val task_id: UUID,
    val name: String,
    val student_id: UUID,
    val student_name: String,
    val inspector_id: UUID? = null,
    val mark: Double? = null,
    val description: String? = null,
    val content: String? = null
                     ) : AbstractEntity()
