package ru.hse.oros.service.entity

import ru.hse.oros.model.entity.Deadline


interface DeadlineService: EntityService<Deadline> {
    fun count(): Int
}