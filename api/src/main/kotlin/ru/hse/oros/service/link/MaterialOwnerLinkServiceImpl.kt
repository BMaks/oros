package ru.hse.oros.service.link

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import ru.hse.oros.dao.link.MaterialOwnerLinkDao
import ru.hse.oros.logger.logger
import ru.hse.oros.model.entity.Material
import ru.hse.oros.model.entity.link.MaterialOwnerLink
import ru.hse.oros.model.entity.link.MaterialOwnerLinkId
import ru.hse.oros.model.request.link.MaterialOwnerLinkRequest
import ru.hse.oros.service.entity.MaterialService
import java.util.*

@Service
class MaterialOwnerLinkServiceImpl(
    @Autowired override val dao: MaterialOwnerLinkDao,
    @Autowired @Lazy override val entityService: MaterialService
                                  ) : AbstractEntityOwnerLinkService<MaterialOwnerLink, MaterialOwnerLinkId, Material, MaterialOwnerLinkRequest>(), MaterialOwnerLinkService {

    override val entityName: String
        get() = "MaterialOwnerLink"

    override val logger
        get() = logger(this)

    override fun newEntityOwnerLink(entityId: UUID, ownerId: UUID): MaterialOwnerLink {
        return MaterialOwnerLink(material_id = entityId, owner_id = ownerId)
    }

}