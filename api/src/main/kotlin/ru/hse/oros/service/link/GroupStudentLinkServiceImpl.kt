package ru.hse.oros.service.link

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import ru.hse.oros.dao.link.GroupStudentLinkDao
import ru.hse.oros.logger.logger
import ru.hse.oros.model.entity.Group
import ru.hse.oros.model.entity.User
import ru.hse.oros.model.entity.link.GroupStudentLink
import ru.hse.oros.model.entity.link.GroupStudentLinkId
import ru.hse.oros.model.request.entity.NotificationSaveRequest
import ru.hse.oros.model.request.link.GroupStudentLinkRequest
import ru.hse.oros.model.request.link.LinkRequest
import ru.hse.oros.service.entity.EntityService
import ru.hse.oros.service.entity.GroupService
import ru.hse.oros.service.entity.NotificationService
import java.util.*

@Service
class GroupStudentLinkServiceImpl(
    @Autowired override val dao: GroupStudentLinkDao,
    @Autowired @Lazy override val entity1Service: EntityService<Group>,
    @Autowired @Lazy override val entity2Service: EntityService<User>,
    @Autowired @Lazy private val groupService: GroupService,
    @Autowired @Lazy private val notificationOwnerLinkService: NotificationOwnerLinkService,
    @Autowired @Lazy private val notificationService: NotificationService
                                 ): AbstractLinkService<GroupStudentLink, GroupStudentLinkId, Group, User, GroupStudentLinkRequest>(), GroupStudentLinkService {

    override val entityName: String
        get() = "GroupStudentLink"

    override val logger
        get() = logger(this)

    override fun newLink(entity1Id: UUID, entity2Id: UUID): GroupStudentLink {
        return GroupStudentLink(entity1Id, entity2Id)
    }

    override fun create(request: LinkRequest) {
        request as GroupStudentLinkRequest
        val oldStudents = dao.findStudentsByGroupId(request.group_id)

        super.create(request)

        notificationOwnerLinkService.shareGroupNotifications(request.group_id, request.student_id)
        notify(request.group_id, request.student_id, oldStudents)
    }

    override fun create(id: UUID, entityIds: List<UUID>) {
        val oldStudents = dao.findStudentsByGroupId(id)

        super.create(id, entityIds)

        notificationOwnerLinkService.shareGroupNotifications(id, entityIds)
        notify(id, entityIds, oldStudents)
    }

    override fun set(id: UUID, entityIds: List<UUID>) {
        logger.info("Set new $entityName with link list and main entity id = $id")

        entity1Service.findById(id)
        val links = mutableListOf<GroupStudentLink>()
        val oldStudents = groupService.getStudentsIds(id)
        val newStudents = entityIds.distinct()

        for (sId in newStudents) {
            entity2Service.findById(sId)
            if (!oldStudents.contains(sId)) links.add(newLink(id, sId))
        }

        val studentsToDelete = oldStudents.minus(newStudents)
        val studentsToSave = newStudents.minus(oldStudents)

        notificationOwnerLinkService.confiscateGroupNotifications(id, studentsToDelete)
        dao.delete(id, studentsToDelete)

        notificationOwnerLinkService.shareGroupNotifications(id, studentsToSave)
        dao.saveAll(links)

        notify(id, entityIds, oldStudents)
    }

    override fun delete(request: LinkRequest) {
        super.delete(request)
        request as GroupStudentLinkRequest
        notificationOwnerLinkService.confiscateGroupNotifications(request.group_id, request.student_id)
    }

    override fun delete(id: UUID, entityIds: List<UUID>) {
        super.delete(id, entityIds)
        notificationOwnerLinkService.confiscateGroupNotifications(id, entityIds)
    }

    override fun deleteGroup(id: UUID) = deleteEntity1(id)
    override fun deleteStudent(id: UUID) = deleteEntity2(id)

    fun notify(groupId: UUID, studentId: UUID, oldStudents: List<UUID>) {
        if (!oldStudents.contains(studentId)) {
            notificationService.create(
                NotificationSaveRequest(
                    author = "Система",
                    msg = "Вы присоединились к группе: ${(groupService.findById(groupId) as Group).name}"
                                       ),
                studentId
                                      )
        }
    }

    fun notify(groupId: UUID, studentIds: List<UUID>, oldStudents: List<UUID>) {
        notificationService.create(
            NotificationSaveRequest(
                author = "Система",
                msg = "Вы присоединились к группе: ${(groupService.findById(groupId) as Group).name}"
                                   ),
            studentIds.minus(oldStudents)
                                  )
    }
}