package ru.hse.oros.service

import ru.hse.oros.model.News
import ru.hse.oros.model.request.NewsSaveRequest
import ru.hse.oros.model.request.entity.EntityRequest
import ru.hse.oros.service.entity.EntityService
import java.util.*


interface NewsService: EntityService<News> {
    fun update(id: UUID, request: NewsSaveRequest)
    override fun update(id: UUID, request: EntityRequest) = this.update(id, request as NewsSaveRequest)

    fun create(request: NewsSaveRequest): News
    override fun create(request: EntityRequest): News = this.create(request as NewsSaveRequest)
}