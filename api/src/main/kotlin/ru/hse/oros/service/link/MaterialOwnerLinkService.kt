package ru.hse.oros.service.link

import ru.hse.oros.model.entity.link.MaterialOwnerLink
import java.util.*


interface MaterialOwnerLinkService: EntityOwnerLinkService<MaterialOwnerLink>