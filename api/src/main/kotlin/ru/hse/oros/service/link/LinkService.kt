package ru.hse.oros.service.link

import ru.hse.oros.model.request.link.LinkRequest
import java.util.*

interface LinkService<T> {
    val entityName: String

    //    fun create(links: List<LinkRequest>) // TODO
    fun set(id: UUID, entityIds: List<UUID>)
    fun create(id: UUID, entityIds: List<UUID>)
    fun create(request: LinkRequest)
    fun delete(request: LinkRequest)
    fun delete(id: UUID, entityIds: List<UUID>)
    fun deleteEntity1(id: UUID)
    fun deleteEntity2(id: UUID)
}