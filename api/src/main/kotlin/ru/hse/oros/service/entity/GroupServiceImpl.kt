package ru.hse.oros.service.entity

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import ru.hse.oros.dao.entity.GroupDao
import ru.hse.oros.exception.EntityNotFountException
import ru.hse.oros.logger.logger
import ru.hse.oros.model.entity.Group
import ru.hse.oros.model.request.entity.EntityRequest
import ru.hse.oros.model.request.entity.GroupSaveRequest
import ru.hse.oros.model.request.link.GroupStudentLinkRequest
import ru.hse.oros.model.response.UserResponse
import ru.hse.oros.service.link.GroupStudentLinkService
import ru.hse.oros.service.link.MaterialOwnerLinkService
import ru.hse.oros.service.link.NotificationOwnerLinkService
import ru.hse.oros.service.link.TaskOwnerLinkService
import ru.hse.oros.utils.asMap
import ru.hse.oros.utils.toUserResponse
import java.util.*

@Service
class GroupServiceImpl(
    @Autowired override val dao: GroupDao,
    @Autowired @Lazy private val userService: UserService,
    @Autowired @Lazy private val taskOwnerLinkService: TaskOwnerLinkService,
    @Autowired @Lazy private val materialOwnerLinkService: MaterialOwnerLinkService,
    @Autowired @Lazy private val notificationOwnerLinkService: NotificationOwnerLinkService,
    @Autowired @Lazy private val groupStudentLinkService: GroupStudentLinkService
                      ) : AbstractEntityService<Group>(), GroupService {


    override val entityName: String
        get() = "Group"

    override val logger
        get() = logger(this)


    override fun findAll(params: Map<String, Any?>): List<Any> {
        val client = security.getClient()

        val groups = if (params["showAll"] == true) {
            dao.findDistinctByOrderByDateAsc()
        } else {
            if (security.isOROS(client.role)) {
                dao.findDistinctByOrderByDateAsc()
            } else {
                dao.findStudentGroupByOrderByName(client.id)
            }
        }

        val response = mutableListOf<MutableMap<String, Any?>>()

        return if (params["showStudents"] == true) {
            for (g in groups) {
                response.add(g.asMap().apply { this["students"] = dao.getGroupStudents(g.id).toUserResponse() })
            }
            response
        } else {
            groups
        }
    }

    override fun daoFindAll(): List<Group> {
        val client = security.getClient()

        return if (security.isOROS(client.role)) {
            dao.findDistinctByOrderByDateAsc()
        } else {
            dao.findStudentGroupByOrderByName(client.id)
        }
    }

    override fun findAllWithStudents(): List<Any> {
        val groups = daoFindAll()
        val response = mutableListOf<MutableMap<String, Any?>>()

        for (g in groups) {
            response.add(g.asMap().apply { this["students"] = dao.getGroupStudents(g.id).toUserResponse() })
        }

        return response
    }

    override fun getStudentsIds(id: UUID): List<UUID> {
        return dao.getGroupStudentIds(id)
    }

    override fun addStudent(groupId: UUID, studentId: UUID) {
        dao.findByIdOrNull(groupId) ?: throw EntityNotFountException(groupId, entityName)
        userService.findById(studentId)

        groupStudentLinkService.create(GroupStudentLinkRequest(group_id = groupId, student_id = studentId))
    }

    override fun deleteStudent(groupId: UUID, studentId: UUID) {
        dao.findByIdOrNull(groupId) ?: throw EntityNotFountException(groupId, entityName)
        userService.findById(studentId)

        groupStudentLinkService.delete(GroupStudentLinkRequest(group_id = groupId, student_id = studentId))
    }

    override fun findByIdWithStudents(id: UUID): Any {
        logger.info("Find $entityName (with students) with id=$id")

        val group = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)
        val response = group.asMap()
        response["students"] = dao.getGroupStudents(group.id).toUserResponse()

        return response
    }

    override fun create(request: EntityRequest): Group {
        request as GroupSaveRequest
        logger.info("Create new $entityName with name=${request.name}")

        val entity = dao.save(
                Group(
                        name = request.name,
                        events = request.events
                    )
                    )
        request.links?.let{
            groupStudentLinkService.create(entity.id, it)
        }

        return entity
    }

    override fun newEntity(entity: Group, request: EntityRequest): Group {
        request as GroupSaveRequest

        return entity.copy(
            name = request.name,
            events = request.events
                          )
    }

    override fun delete(id: UUID) {
        logger.info("Delete $entityName with id = $id")

        val entity = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)

        taskOwnerLinkService.deleteOwner(id)
        materialOwnerLinkService.deleteOwner(id)
        notificationOwnerLinkService.deleteOwner(id)
        groupStudentLinkService.deleteGroup(id)

        dao.delete(entity)
    }
}