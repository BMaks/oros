package ru.hse.oros.service

import ru.hse.oros.model.entity.Group
import java.util.*


interface GradebookService {
    val entityName: String

    fun getHeaders():  List<Any>
    fun getGroupHeaders(groupId: UUID):  List<Any>
    fun getGroups():  List<Any>
    fun getGroup(group: Group):  Map<String, Any>
    fun getStudent(id: UUID?):  Map<String, Any?>
    fun getGradeBooks(): Map<String, Any?>
    fun getGroupGradeBook(id: UUID): Map<String, Any?>

    fun createReport(groupId: UUID): ByteArray
}