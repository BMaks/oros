package ru.hse.oros.service.entity

import ru.hse.oros.model.entity.SolvedTask
import ru.hse.oros.model.request.TaskMarkRequest
import java.util.*


interface SolvedTaskService: EntityService<SolvedTask> {
    fun setMark(id: UUID, request: TaskMarkRequest)
    fun deleteStudentTasks(id: UUID)
}