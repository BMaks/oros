package ru.hse.oros.service.entity

import ru.hse.oros.model.entity.AbstractEntity
import ru.hse.oros.model.request.entity.EntityRequest
import java.util.*


interface EntityService<T: AbstractEntity> {
    val entityName: String

    fun findAll(): List<Any>

    fun findById(id: UUID): Any

    fun findByIdOrNull(id: UUID): Any?

    fun create(request: EntityRequest): T

    fun update(id: UUID, request: EntityRequest)

    fun delete(id: UUID)
}

