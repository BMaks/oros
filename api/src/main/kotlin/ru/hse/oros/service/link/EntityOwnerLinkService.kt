package ru.hse.oros.service.link

import java.util.*

interface EntityOwnerLinkService<T>: LinkService<T> {
    fun existsOwner(id: UUID): Boolean
    fun getOwner(id: UUID): Any?
    fun deleteOwner(id: UUID)
    fun deleteEntity(id: UUID)
    override fun deleteEntity1(id: UUID) = deleteEntity(id)
    override fun deleteEntity2(id: UUID) = deleteOwner(id)
}