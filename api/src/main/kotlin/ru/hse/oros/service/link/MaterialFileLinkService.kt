package ru.hse.oros.service.link

import ru.hse.oros.model.entity.link.MaterialFileLink
import java.util.*

interface MaterialFileLinkService: LinkService<MaterialFileLink> {
    fun deleteMaterial(id: UUID)
    fun deleteFile(id: UUID)
}