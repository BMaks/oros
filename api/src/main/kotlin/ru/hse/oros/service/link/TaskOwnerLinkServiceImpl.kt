package ru.hse.oros.service.link

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import ru.hse.oros.dao.link.TaskOwnerLinkDao
import ru.hse.oros.logger.logger
import ru.hse.oros.model.entity.Task
import ru.hse.oros.model.entity.link.TaskOwnerLink
import ru.hse.oros.model.entity.link.TaskOwnerLinkId
import ru.hse.oros.model.request.entity.NotificationSaveRequest
import ru.hse.oros.model.request.link.LinkRequest
import ru.hse.oros.model.request.link.TaskOwnerLinkRequest
import ru.hse.oros.service.entity.NotificationService
import ru.hse.oros.service.entity.TaskService
import java.util.*

@Service
class TaskOwnerLinkServiceImpl(
    @Autowired override val dao: TaskOwnerLinkDao,
    @Autowired @Lazy override val entityService: TaskService,
    @Autowired @Lazy private val notificationService: NotificationService
                              ) : AbstractEntityOwnerLinkService<TaskOwnerLink, TaskOwnerLinkId, Task, TaskOwnerLinkRequest>(), TaskOwnerLinkService {

    override val entityName: String
        get() = "TaskOwnerLink"

    override val logger
        get() = logger(this)

    override fun newEntityOwnerLink(entityId: UUID, ownerId: UUID): TaskOwnerLink {
        return TaskOwnerLink(task_id = entityId, owner_id = ownerId)
    }

    override fun create(request: LinkRequest) {
        request as TaskOwnerLinkRequest
        val oldOwners = dao.findOwnersByTaskId(request.entity_id)

        super.create(request)
        notify(request.entity_id, request.owner_id, oldOwners)
    }

    override fun create(id: UUID, entityIds: List<UUID>) {
        val oldOwners = dao.findOwnersByTaskId(id)
        super.create(id, entityIds)
        notify(id, entityIds, oldOwners)
    }

    override fun set(id: UUID, entityIds: List<UUID>) {
        val oldOwners = dao.findOwnersByTaskId(id)
        super.set(id, entityIds)
        notify(id, entityIds, oldOwners)
    }

    private fun notify(taskId: UUID, ownerId: UUID, oldOwners: List<UUID>) {
        if (!oldOwners.contains(ownerId)) {
            notificationService.create(
                NotificationSaveRequest(
                    author = "Система",
                    msg = "Вам присвоено Задание: ${(entityService.findById(taskId) as Task).name}"
                                       ),
                ownerId
                                      )
        }
    }

    private fun notify(taskId: UUID, ownerIds: List<UUID>, oldOwners: List<UUID>) {
        notificationService.create(
            NotificationSaveRequest(
                author = "Система",
                msg = "Вам присвоено Задание: ${(entityService.findById(taskId) as Task).name}"
                                   ),
            ownerIds.minus(oldOwners)
                                  )
    }

}