package ru.hse.oros.service.link

import ru.hse.oros.model.entity.link.NotificationOwnerLink
import java.util.*


interface NotificationOwnerLinkService: EntityOwnerLinkService<NotificationOwnerLink> {
    fun setIfViewed(notificationIds: List<UUID>, ownerID: UUID, isViewed: Boolean = true)
    fun setIfViewed(notificationIds: List<UUID>, isViewed: Boolean = true)
    fun shareGroupNotifications(groupId: UUID, studentId: UUID)
    fun shareGroupNotifications(groupId: UUID, studentIds: List<UUID>)
    fun confiscateGroupNotifications(groupId: UUID, studentId: UUID)
    fun confiscateGroupNotifications(groupId: UUID, studentIds: List<UUID>)
}