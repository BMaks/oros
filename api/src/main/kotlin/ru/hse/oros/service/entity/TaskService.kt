package ru.hse.oros.service.entity

import ru.hse.oros.model.entity.Task
import ru.hse.oros.model.request.entity.EntityRequest
import java.util.*


interface TaskService: EntityService<Task> {
    fun addDeadline(taskId: UUID, deadlineId: UUID)
    fun deleteDeadline(taskId: UUID, deadlineId: UUID)
}