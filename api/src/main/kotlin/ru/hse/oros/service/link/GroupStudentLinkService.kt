package ru.hse.oros.service.link

import ru.hse.oros.model.entity.link.GroupStudentLink
import java.util.*

interface GroupStudentLinkService: LinkService<GroupStudentLink> {
    fun deleteGroup(id: UUID)
    fun deleteStudent(id: UUID)
}