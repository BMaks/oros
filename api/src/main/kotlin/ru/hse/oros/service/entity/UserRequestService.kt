package ru.hse.oros.service.entity

import ru.hse.oros.model.entity.UserRequest
import ru.hse.oros.model.request.entity.UserRequestRegisterRequest
import java.util.*


interface UserRequestService: EntityService<UserRequest> {
    fun submit(id: UUID)
    fun requestNewUser(request: UserRequestRegisterRequest)
}