package ru.hse.oros.service.entity

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import ru.hse.oros.dao.entity.MaterialDao
import ru.hse.oros.exception.EntityNotFountException
import ru.hse.oros.model.entity.Material
import ru.hse.oros.logger.logger
import ru.hse.oros.model.entity.File
import ru.hse.oros.model.request.entity.MaterialSaveRequest
import ru.hse.oros.model.request.entity.EntityRequest
import ru.hse.oros.service.link.MaterialOwnerLinkService
import ru.hse.oros.utils.asMap
import java.util.*

@Service
class MaterialServiceImpl(
        @Autowired override val dao: MaterialDao,
        @Autowired private val materialOwnerLinkService: MaterialOwnerLinkService
                         ) : AbstractEntityService<Material>(), MaterialService {

    override val entityName: String
        get() = "Material"

    override val logger
        get() = logger(this)

    override fun getFiles(id: UUID): List<File> {
        logger.info("Find $entityName files with id=$id")
        dao.findById(id)
        return dao.findMaterialFiles(id)
    }

    override fun getFilesToResponse(id: UUID): List<Map<String, Any?>> {
        logger.info("Find $entityName files with id=$id")
        dao.findById(id)

        val files = dao.findMaterialFiles(id).map { file ->
            file.asMap().apply { this.remove("path") }
        }

        return files
    }

    override fun daoFindAll(): List<Material> {
        logger.info("Find all $entityName")

        val client = security.getClient()

        return if (security.isOROS(client.role))
            dao.findDistinctByOrderByDateAsc()
        else
            dao.findStudentMaterials(client.id)
    }

    override fun create(request: EntityRequest): Material {
        request as MaterialSaveRequest
        logger.info("Create new $entityName with name=${request.name}")

        val entity = dao.save(
            Material(
                name = request.name,
                description = request.description,
                option = request.option,
                active = request.active
                    )
                       )

        request.links?.let{
             materialOwnerLinkService.create(entity.id, it)
        }

        return entity
    }

    override fun newEntity(entity: Material, request: EntityRequest): Material {
        request as MaterialSaveRequest

        return entity.copy(
            name = request.name,
            description = request.description,
            option = request.option,
            active = request.active
                          )
    }

    override fun delete(id: UUID) {
        logger.info("Delete $entityName with id = $id")

        val entity = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)
        materialOwnerLinkService.deleteEntity(id)
        dao.delete(entity)
    }

}