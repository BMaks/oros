package ru.hse.oros.service.entity

import ru.hse.oros.model.entity.Group
import java.util.*


interface GroupService: EntityService<Group> {
    fun getStudentsIds(id: UUID): List<UUID>
    fun findAllWithStudents(): List<Any>
    fun findByIdWithStudents(id: UUID): Any
    fun addStudent(groupId: UUID, studentId: UUID)
    fun deleteStudent(groupId: UUID, studentId: UUID)
    fun findAll(params: Map<String, Any?>): List<Any>
}