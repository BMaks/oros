package ru.hse.oros.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import ru.hse.oros.dao.entity.GroupDao
import ru.hse.oros.dao.entity.SolvedTaskDao
import ru.hse.oros.dao.entity.TaskDao
import ru.hse.oros.exception.EntityNotFountException
import ru.hse.oros.logger.logger
import ru.hse.oros.model.entity.Group
import ru.hse.oros.model.entity.Task
import ru.hse.oros.model.entity.User
import ru.hse.oros.security.SecurityService
import ru.hse.oros.service.entity.UserService
import java.util.*

@Service
class GradebookServiceImpl(
    @Autowired @Lazy private val groupDao: GroupDao,
    @Autowired @Lazy private val solvedTaskDao: SolvedTaskDao,
    @Autowired @Lazy private val taskDao: TaskDao,
    @Autowired @Lazy private val userService: UserService,
    @Autowired @Lazy private val security: SecurityService,
    @Autowired @Lazy private val apachePoiService: ApachePoiService
                          ) : GradebookService {

    override val entityName: String
        get() = "Gradebook"

    val logger = logger(this)


    override fun getHeaders(): List<Any> {
        val client = security.getClient()
        val groups = getClientGroups(client)

        val tasks = taskDao.findOwnerTasks(groups.map { it.id })
        return getHeaders(tasks)
    }

    override fun getGroupHeaders(groupId: UUID): List<Any> {
        val tasks = taskDao.findGroupTasks(groupId)
        return getHeaders(tasks)
    }

    private fun getHeaders(tasks: List<Task>): List<Any> {
        val response = tasks.map { task ->
            mapOf(
                "id" to task.id,
                "name" to task.name
                 )
        }.toMutableList()
            .apply { add(mapOf("id" to "final", "name" to "Итог")) }

        return response
    }

    override fun getGroups(): List<Any> {
        val client = security.getClient()

        val groups = if (security.isOROS(client.role)) {
            groupDao.findDistinctByOrderByName()
        } else {
            groupDao.findStudentGroupByOrderByName(client.id)
        }

        return getGroups(groups)
    }

    private fun getGroups(groups: List<Group>): List<Any> {
        return groups.map { getGroup(it) }
    }

    override fun getGroup(group: Group): Map<String, Any> {
        return mapOf(
            "id" to group.id,
            "name" to group.name,
            "students" to run {
                groupDao.getGroupStudents(group.id).map { user ->
                    mapOf(
                        "id" to user.id,
                        "name" to user.name,
                        "marks" to let {
                            solvedTaskDao.findStudentSolvedTasks(user.id).map { task ->
                                mapOf(
                                    "id" to task.task_id,
                                    "mark" to task.mark
                                     )
                            }.toMutableList().apply {
                                var finalMark =
                                    this.sumOf { (it["mark"] as Double?) ?: let { 0.0 } } / this.count()
                                if (finalMark.isNaN()) finalMark = 0.0
                                add(mapOf("id" to "final", "mark" to finalMark))
                            }
                        }
                         )
                }
            }
             )
    }

    override fun getGroupGradeBook(id: UUID): Map<String, Any?> {
        val group = groupDao.findByIdOrNull(id) ?: throw EntityNotFountException(id, "Group")

        return mapOf(
            "group" to getGroup(group),
            "headers" to getGroupHeaders(group.id)
                    )
    }

    override fun getStudent(id: UUID?): Map<String, Any?> {
        val student = id?.let { userService.findById(id) as User } ?: let { security.getClient() }
        return getStudent(student)
    }

    private fun getStudent(student: User): Map<String, Any?> {
        return mapOf(
            "id" to student.id,
            "name" to student.name,
            "marks" to solvedTaskDao.findStudentSolvedTasks(student.id).map { task ->
                mapOf(
                    "id" to task.task_id,
                    "name" to task.name,
                    "mark" to task.mark
                     )

            }
                    )
    }

    override fun getGradeBooks(): Map<String, Any?> {
        val client = security.getClient()
        val groups = getClientGroups(client)

        val gradebook = mutableMapOf<String, Any?>()

        gradebook["groups"] = getGroups(groups)
        gradebook["headers"] = getHeaders(taskDao.findOwnerTasks(groups.map { it.id }))

        if (!security.isOROS(client.role)) {
            gradebook["student"] = getStudent(client)
        }

        return gradebook
    }

    private fun getClientGroups(client: User): List<Group> {
        return if (security.isOROS(client.role)) {
            groupDao.findDistinctByOrderByName()
        } else {
            groupDao.findStudentGroupByOrderByName(client.id)
        }
    }

    override fun createReport(groupId: UUID): ByteArray {
        val gradebook = getGroupGradeBook(groupId)
        return apachePoiService.createGradebookReport(gradebook)
    }
}