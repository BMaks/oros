import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.4.5"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    kotlin("jvm") version "1.4.32"
    kotlin("plugin.spring") version "1.4.32"
    kotlin("plugin.jpa") version "1.4.32"
}

group = "ru.hse"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_10
java.targetCompatibility = JavaVersion.VERSION_1_10

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

repositories {
    mavenCentral()
}

extra["azureVersion"] = "3.3.0"
extra["springCloudVersion"] = "2020.0.2"

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-security")

    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    runtimeOnly("com.h2database:h2")
//    implementation("org.springframework.boot:spring-boot-starter-jooq")
//    runtimeOnly("org.postgresql:postgresql")


    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    developmentOnly("org.springframework.boot:spring-boot-devtools")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
    testImplementation("org.springframework.boot:spring-boot-starter-test")

    implementation("org.apache.poi:poi:4.1.2")
    implementation("org.apache.poi:poi-ooxml:4.1.2")


    // analysis
//    implementation("org.springframework.boot:spring-boot-starter-actuator")

    // TODO evil ruins app
//    implementation("org.springframework.cloud:spring-cloud-sleuth-zipkin")
//    implementation("org.springframework.cloud:spring-cloud-starter-config")
//    implementation("org.springframework.cloud:spring-cloud-starter-netflix-eureka-server") // TODO eureka
//    implementation("org.springframework.cloud:spring-cloud-starter-sleuth")
}

dependencyManagement {
    imports {
        mavenBom("com.azure.spring:azure-spring-boot-bom:${property("azureVersion")}")
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
    }
}

/*
tasks.register("mkFDir") {
    doLast {
        mkdir("$projectDir/fdir")
        println("run mkfdir")
    }
}*/

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "10"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}




/*tasks.register<Copy>("copyTests") {
    from(layout.buildDirectory.dir("classes/kotlin/test/**"))
    into(layout.buildDirectory.dir("classes/java"))
}*/


//@Tag("unitTest")

useJUnitPlatform {
        includeTags 'unitTest'
    }
